SRC_DIR = src
TARGET_DIR = ${SRC_DIR}/target
TEST_DIR = test

FLEX_ARGS = -o ./${TARGET_DIR}/lex.yy.c
BISON_ARGS = -l --defines=./${TARGET_DIR}/c--parserspec.tab.h --output=./${TARGET_DIR}/c--parserspec.tab.c

all: createtarget runbison runflex compileprogram

compile: all
	cp ./${TARGET_DIR}/compile .

.PHONY: clean
clean:
	if [ -f ./compile ]; then rm ./compile; fi
	if [ -d ./${TARGET_DIR} ]; then rm -rf ./${TARGET_DIR}; fi
	if [ -f ./${TEST_DIR}/test-runner ]; then rm ./${TEST_DIR}/test-runner; fi

createtarget:
	mkdir -p ./${TARGET_DIR}

runbison:
	bison ${BISON_ARGS} ./${SRC_DIR}/c--parserspec.y

runflex:
	flex ${FLEX_ARGS} ./${SRC_DIR}/c--scannerspec.l

compileprogram:
	gcc -g -O0 ./${SRC_DIR}/main.c ./${SRC_DIR}/symboltable.c ./${SRC_DIR}/parsetree.c ./${SRC_DIR}/ir_code.c ./${TARGET_DIR}/lex.yy.c ./${TARGET_DIR}/c--parserspec.tab.c -o ./${TARGET_DIR}/compile

test: all
	g++ -g -O0 --std=c++11 -Wno-write-strings ./${SRC_DIR}/symboltable.c ./${SRC_DIR}/parsetree.c ./${SRC_DIR}/ir_code.c ./${TARGET_DIR}/lex.yy.c ./${TARGET_DIR}/c--parserspec.tab.c ./${TEST_DIR}/catch-runner.o ./${TEST_DIR}/catch-tests-lexer.cpp ./${TEST_DIR}/catch-tests-symboltable.cpp -o ./${TEST_DIR}/test-runner
	$(MAKE) -C test test

debug: FLEX_ARGS += -d
debug: BISON_ARGS += -v
debug: all 
