# CSC453-compiler

[![pipeline status](https://gitlab.com/mackncheesiest/CSC453-compiler/badges/master/pipeline.svg)](https://gitlab.com/mackncheesiest/CSC453-compiler/commits/master)

C-- Compiler associated with CSC453 course from UA http://www2.cs.arizona.edu/classes/cs453/fall18/

Dependencies:
- make
- flex
- bison
- [Catch2](https://github.com/catchorg/Catch2)
