%code requires {
    #include <stdio.h>
    #include <string.h>
    #include "../symboltable.h"
    #include "../parsetree.h"
    #include "../ir_code.h"
}

%initial-action {
    SymbolTable_free(st, true);
    st = SymbolTable_new();
    localFunc = NULL;
    strTab = NULL;
    seenReturn = false;
    encounteredParsingError = 0;
    inInitialState = 1;

    initializeOutput(outputFile);
}

%{
    #include <stdio.h>
    #include <string.h>
    #include "../symboltable.h"
    #include "../parsetree.h"
    #include "../ir_code.h"
    extern int yylex();
    extern int yylineno;
    extern char* yytext;
    extern int inInitialState;
    void yyerror(const char* s);
    int encounteredParsingError;
    Symbol* st;
    Symbol* localFunc;
    StringTable* strTab;
    FILE* outputFile;
    bool seenReturn;
    bool isInvalidSymbolDecl(Symbol* root, Symbol* symbol);
    bool isInvalidFuncDecl(Symbol* root, Symbol* func);
    bool typesAreCompatible(SymbolType type1, SymbolType type2);
    SymbolType getArrTypeForType(SymbolType type);
    SymbolType getTypeForArrType(SymbolType type);
%}

/* Enable more verbose default error reporting  https://www.gnu.org/software/bison/manual/html_node/Error-Reporting.html */
%define parse.error verbose

%union{
    SymbolType typeEnum;
    int intVal;
    char charVal;
    char* stringVal;
    Symbol* symbolPtr;
    Node* nodePtr;
}

%token <symbolPtr> ID "identifier"
%token <intVal> INTCON "integer constant"
%token <charVal> CHARCON "char constant"
%token <stringVal> STRINGCON "string constant"

%token <typeEnum> INT_KEYWD "int"
%token <typeEnum> CHAR_KEYWD "char"
%token <typeEnum> EXTERN_KEYWD "extern"
%token <typeEnum> VOID_KEYWD "void"
%type <typeEnum> type

%token <nodePtr> IF_KEYWD "if"
%token <nodePtr> ELSE_KEYWD "else"
%token <nodePtr> FOR_KEYWD "for"
%token <nodePtr> WHILE_KEYWD "while"
%token <nodePtr> RETURN_KEYWD "return"

%token <nodePtr> EQ_RELOP "=="
%token <nodePtr> NEQ_RELOP "!="
%token <nodePtr> LTE_RELOP "<="
%token <nodePtr> LT_RELOP "<"
%token <nodePtr> GTE_RELOP ">="
%token <nodePtr> GT_RELOP ">"

%token <nodePtr> AND_LOGICOP "&&"
%token <nodePtr> OR_LOGICOP "||"

%type <nodePtr> expr expr_rpt
%type <nodePtr> func func_stmts
%type <nodePtr> stmt stmt_rpt
%type <nodePtr> assg
%type <nodePtr> for_sub_1 for_sub_2 for_sub_3

%type <symbolPtr> var_decl decl_var_decl_rpt parm_types decl_parm_types_rpt func_vardecls

%start Prog

/* TODO: I don't think * and / have proper higher precedance than +/- : See syntax tree construction */
%left OR_LOGICOP
%left AND_LOGICOP
%left EQ_RELOP NEQ_RELOP
%left LT_RELOP LTE_RELOP GT_RELOP GTE_RELOP
%left '+' '-'
%left '*' '/'

%right '!'
/* 
    Solve the unary minus issue by declaring a fictitious token and using its precedence where needed 
    https://www.gnu.org/software/bison/manual/bison.html#Contextual-Precedence
*/
%right UMINUS

%%

Prog: 
    /* empty */ {
        if (!inInitialState) { 
            yyerror("Reached EOF with unfinished block comment"); 
        } 
        if (!inInitialState || encounteredParsingError) { 
            YYABORT;
        }
        generateGlobalCode(st, strTab, outputFile);
    }
    | decl Prog { 
        if (!inInitialState) { 
            yyerror("Reached EOF with unfinished block comment"); 
        } 
        if (!inInitialState || encounteredParsingError) { 
            YYABORT;
        }
    }
    | func Prog { 
        if (!inInitialState) { 
            yyerror("Reached EOF with unfinished block comment"); 
        } 
        if (!inInitialState || encounteredParsingError) { 
            YYABORT;
        }
    }
    | error { YYABORT; }
    ;

decl:
    type var_decl decl_var_decl_rpt ';' {
        /* Set the final type for $2 */
        if ($2->type == ARR_TYPE) {
            Symbol_setType($2, getArrTypeForType($1));
        } else {
            Symbol_setType($2, $1);
        }

        /* Set the final types for each symbol in $3 */
        Symbol* walker = $3;
        while (walker != NULL) {
            if (walker->type == ARR_TYPE) {
                Symbol_setType(walker, getArrTypeForType($1));
            } else {
                Symbol_setType(walker, $1);
            }
            walker = walker->nextSymbol;
        }

        /* Create a sublist */
        SymbolTable_append($2, $3);

        /* Validate each of the symbol declarations before adding them to the real symbol table */
        walker = $2;
        while (walker != NULL) {
            if (isInvalidSymbolDecl(st, walker)) {
                if (walker == $2) {
                    $2 = SymbolTable_remove($2, $2->name);
                    walker = $2;
                } else {
                    Symbol* next = walker->nextSymbol;
                    SymbolTable_remove($2, walker->name);
                    walker = next;
                }
            } else {
                Symbol* copySym = Symbol_copy(walker);
                SymbolTable_append(st, copySym);
            }
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }
        /* Free whatever sublist remains since all relevant entries have been Symbol_copy'd */
        // SymbolTable_free($2, true);
    }
    | type ID '(' parm_types ')' decl_parm_types_rpt ';' {
        /* Process the function declaration symbol from $2 */
        Symbol_setType($2, FUNC_NODEF_TYPE);
        Symbol_setArgSymbol($2, $4);
        Symbol_setReturnType($2, $1);

        /* Process the list of function declarations from $6 */
        Symbol* walker = $6;
        while (walker != NULL) {
            Symbol_setType($2, FUNC_NODEF_TYPE);
            Symbol_setReturnType(walker, $1);
            walker = walker->nextSymbol;
        }

        /* Create a sublist */
        SymbolTable_append($2, $6);

        /* Validate each of the functions before adding them to the symbol table */
        walker = $2;
        while (walker != NULL) {
            if (isInvalidFuncDecl(st, walker)) {
                if (walker == $2) {
                    $2 = SymbolTable_remove($2, $2->name);
                    walker = $2;
                } else {
                    Symbol* next = walker->nextSymbol;
                    SymbolTable_remove($2, walker->name);
                    walker = next;
                }
            } else {
                /* Still need to iteratively append to the symbol table in case two */
                Symbol* copySym = Symbol_copy(walker);
                Symbol_setArgSymbol(copySym, walker->function_arg);
                SymbolTable_append(st, copySym);
            }
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }
        // fprintf(stderr, "[decl] Current state of symbol table:\n");
        // SymbolTable_print(st);
        /* Free whatever sublist remains since all relevant entries have been Symbol_copy'd */
        // SymbolTable_free($2, true);
    }
    | EXTERN_KEYWD type ID '(' parm_types ')' decl_parm_types_rpt ';' {
        /* Process the function declaration symbol from $2 */
        Symbol_setType($3, EXTERN_FUNC_NODEF_TYPE);
        Symbol_setArgSymbol($3, $5);
        Symbol_setReturnType($3, $2);

        /* Process the list of function declarations from $6 */
        Symbol* walker = $7;
        while (walker != NULL) {
            Symbol_setType(walker, EXTERN_FUNC_NODEF_TYPE);
            Symbol_setReturnType(walker, $2);
            walker = walker->nextSymbol;
        }

        /* Create a sublist */
        SymbolTable_append($3, $7);

        /* Validate each of the functions before adding them to the symbol table */
        walker = $3;
        while (walker != NULL) {
            if (isInvalidFuncDecl(st, walker)) {
                if (walker == $3) {
                    $3 = SymbolTable_remove($3, $3->name);
                    walker = $3;
                } else {
                    Symbol* next = walker->nextSymbol;
                    SymbolTable_remove($3, walker->name);
                    walker = next;
                }
            } else {
                /* Still need to iteratively append to the symbol table in case two */
                Symbol* copySym = Symbol_copy(walker);
                Symbol_setArgSymbol(copySym, walker->function_arg);
                SymbolTable_append(st, copySym);
            }
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }
        // fprintf(stderr, "[decl] Current state of symbol table:\n");
        // SymbolTable_print(st);
        /* Free whatever sublist remains since all relevant entries have been Symbol_copy'd */
        // SymbolTable_free($3, true);
    }
    | VOID_KEYWD ID '(' parm_types ')' decl_parm_types_rpt ';' {
        /* Process the function declaration symbol from $2 */
        Symbol_setType($2, FUNC_NODEF_TYPE);
        Symbol_setArgSymbol($2, $4);
        Symbol_setReturnType($2, VOID_TYPE);

        /* Process the list of function declarations from $6 */
        Symbol* walker = $6;
        while (walker != NULL) {
            Symbol_setType(walker, FUNC_NODEF_TYPE);
            Symbol_setReturnType(walker, VOID_TYPE);
            walker = walker->nextSymbol;
        }

        /* Create a sublist */
        SymbolTable_append($2, $6);

        /* Validate each of the functions before adding them to the symbol table */
        walker = $2;
        while (walker != NULL) {
            if (isInvalidFuncDecl(st, walker)) {
                if (walker == $2) {
                    $2 = SymbolTable_remove($2, $2->name);
                    walker = $2;
                } else {
                    Symbol* next = walker->nextSymbol;
                    SymbolTable_remove($2, walker->name);
                    walker = next;
                }
            } else {
                /* Still need to iteratively append to the symbol table in case two */
                Symbol* copySym = Symbol_copy(walker);
                Symbol_setArgSymbol(copySym, walker->function_arg);
                SymbolTable_append(st, copySym);
            }
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }
        // fprintf(stderr, "[decl] Current state of symbol table:\n");
        // SymbolTable_print(st);
        /* Free whatever sublist remains since all relevant entries have been Symbol_copy'd */
        // SymbolTable_free($2, true);
    }
    | EXTERN_KEYWD VOID_KEYWD ID '(' parm_types ')' decl_parm_types_rpt ';' {
        /* Process the function declaration symbol from $2 */
        Symbol_setType($3, EXTERN_FUNC_NODEF_TYPE);
        Symbol_setArgSymbol($3, $5);
        Symbol_setReturnType($3, VOID_TYPE);

        /* Process the list of function declarations from $6 */
        Symbol* walker = $7;
        while (walker != NULL) {
            Symbol_setType(walker, EXTERN_FUNC_NODEF_TYPE);
            Symbol_setReturnType(walker, VOID_TYPE);
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }

        /* Create a sublist */
        SymbolTable_append($3, $7);

        /* Validate each of the functions before adding them to the symbol table */
        walker = $3;
        while (walker != NULL) {
            if (isInvalidFuncDecl(st, walker)) {
                if (walker == $3) {
                    $3 = SymbolTable_remove($3, $3->name);
                    walker = $3;
                } else {
                    Symbol* next = walker->nextSymbol;
                    SymbolTable_remove($3, walker->name);
                    walker = next;
                }
            } else {
                /* Still need to iteratively append to the symbol table in case two */
                Symbol* copySym = Symbol_copy(walker);
                Symbol_setArgSymbol(copySym, walker->function_arg);
                SymbolTable_append(st, copySym);
            }
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }
        // fprintf(stderr, "[decl] Current state of symbol table:\n");
        // SymbolTable_print(st);
        /* Free whatever sublist remains since all relevant entries have been Symbol_copy'd */
        // SymbolTable_free($3, true);
    }
    ;

decl_parm_types_rpt:
    /* empty */ { $$ = NULL; }
    | ',' ID '(' parm_types ')' decl_parm_types_rpt {
        Symbol_setType($2, FUNC_NODEF_TYPE);
        Symbol_setArgSymbol($2, $4);
        SymbolTable_append($2, $6);
        $$ = $2;
    }
    ;

var_decl:
    ID  {
        /* Set yylval.symbolPtr in lexer */ 
        Symbol_setType($1, NONARR_TYPE);
        $$ = $1;
    }
    | ID '[' INTCON ']' { 
        /* Set yylval.symbolPtr in lexer */
        Symbol_setType($1, ARR_TYPE); 
        Symbol_setNumArrElements($1, $3);
        $$ = $1;
    }
    ;

decl_var_decl_rpt:
    /* empty */ { $$ = NULL; }
    | ',' var_decl decl_var_decl_rpt { SymbolTable_append($2, $3); $$ = $2; }
    ;

type:
    INT_KEYWD  { $$ = INT_TYPE; }
    | CHAR_KEYWD  { $$ = CHAR_TYPE; }
    | error { $$ = ERROR_TYPE; }
    ;

parm_types:
    VOID_KEYWD { $$ = Symbol_new("*void*", VOID_TYPE); }
    | type ID   { Symbol_setType($2, $1); $$ = $2; }
    | type ID '[' ']' { Symbol_setType($2, getArrTypeForType($1)); $$ = $2; } 
    | type ID ',' parm_types { 
        Symbol_setType($2, $1); 
        char buffer[128];
        if (SymbolTable_contains($4, $2->name)) {
            snprintf(buffer, sizeof(buffer), "Found repeated declaration of function parameter [%s]", $2->name);
            yyerror(buffer);
        } else {
            SymbolTable_append($2, $4);
        }
        $$ = $2; 
    }
    | type ID '[' ']' ',' parm_types { 
        Symbol_setType($2, getArrTypeForType($1)); 
        char buffer[128];
        if (SymbolTable_contains($6, $2->name)) {
            snprintf(buffer, sizeof(buffer), "Found repeated declaration of function parameter [%s]", $2->name);
            yyerror(buffer);
        } else {
            SymbolTable_append($2, $6);
        }
        $$ = $2; 
        /* TODO: Make sure this works with more than one parameter per function! */ 
    }
    ;

func:
    type ID '(' parm_types ')' '{' {
        /* Process the function declaration symbol from $2 */
        Symbol_setType($2, FUNC_HASDEF_TYPE);
        Symbol_setArgSymbol($2, $4);
        Symbol_setReturnType($2, $1);
        if (isInvalidFuncDecl(st, $2)) {
            /* NO-OP since already reported in the helper method? */
        }
        Symbol* symbol = SymbolTable_get(st, $2->name);
        if (symbol == NULL) {
            symbol = $2;
        } else {
            Symbol_setType(symbol, FUNC_HASDEF_TYPE);
            Symbol_setArgSymbol(symbol, $4);
            Symbol_setReturnType(symbol, $1);
        }
        symbol->local_st = SymbolTable_new();
        Symbol* parmWalker = $4;
        while (parmWalker != NULL) {
            Symbol* copy = Symbol_copy(parmWalker);
            SymbolTable_append(symbol->local_st, copy);
            parmWalker = parmWalker->nextSymbol;
        }
        if (SymbolTable_get(st, symbol->name) == NULL) {
            SymbolTable_append(st, symbol);
        }
        localFunc = symbol;
    } func_vardecls func_stmts '}' { 
        if (seenReturn == false) { 
            char buffer[128];
            snprintf(buffer, sizeof(buffer), "Expected return statement in function [%s] with return type [%s]", localFunc->name, SymbolType_toStr(localFunc->function_returnType));
            yyerror(buffer);
        }
        generateFunction($9, st, localFunc, outputFile);
        seenReturn = false;
        localFunc = NULL;
        // fprintf(stderr, "[func] Just finished processing a function with the following local syntax tree:\n");
        // Tree_print($9, 0);
        $$ = NULL;
    }
    | VOID_KEYWD ID '(' parm_types ')' '{' {
        /* Process the function declaration symbol from $2 */
        Symbol_setType($2, FUNC_HASDEF_TYPE);
        Symbol_setArgSymbol($2, $4);
        Symbol_setReturnType($2, $1);
        if (isInvalidFuncDecl(st, $2)) {
            /* NO-OP? */
        }
        Symbol* symbol = SymbolTable_get(st, $2->name);
        if (symbol == NULL) {
            symbol = $2;
        } else {
            Symbol_setType(symbol, FUNC_HASDEF_TYPE);
            Symbol_setArgSymbol(symbol, $4);
            Symbol_setReturnType(symbol, $1);
        }
        symbol->local_st = SymbolTable_new();
        Symbol* parmWalker = $4;
        while (parmWalker != NULL) {
            Symbol* copy = Symbol_copy(parmWalker);
            SymbolTable_append(symbol->local_st, copy);
            parmWalker = parmWalker->nextSymbol;
        }
        if (SymbolTable_get(st, symbol->name) == NULL) {
            SymbolTable_append(st, symbol);
        }
        localFunc = symbol;
    } func_vardecls func_stmts '}' { 
        generateFunction($9, st, localFunc, outputFile);
        seenReturn = false; 
        localFunc = NULL;
        // fprintf(stderr, "[func] Just finished processing a function with the following local syntax tree:\n");
        // Tree_print($9, 0); 
        $$ = NULL; 
    }
    ;

func_vardecls:
    /* empty */ { $$ = NULL; }
    | type var_decl decl_var_decl_rpt ';' func_vardecls {
        /* Set the final type for $2 */
        if ($2->type == ARR_TYPE) {
            Symbol_setType($2, getArrTypeForType($1));
        } else {
            Symbol_setType($2, $1);
        }

        /* Set the final types for each symbol in $3 */
        Symbol* walker = $3;
        while (walker != NULL) {
            if (walker->type == ARR_TYPE) {
                Symbol_setType(walker, getArrTypeForType($1));
            } else {
                Symbol_setType(walker, $1);
            }
            walker = walker->nextSymbol;
        }

        /* Create a sublist */
        SymbolTable_append($2, $3);

        /* Validate each of the symbol declarations before adding them to the real symbol table */
        walker = $2;
        while (walker != NULL) {
            if (isInvalidSymbolDecl(localFunc->local_st, walker)) {
                if (walker == $2) {
                    $2 = SymbolTable_remove($2, $2->name);
                    walker = $2;
                } else {
                    Symbol* next = walker->nextSymbol;
                    SymbolTable_remove($2, walker->name);
                    walker = next;
                }
            } else {
                Symbol* copySym = Symbol_copy(walker);
                copySym->nextSymbol = NULL;
                SymbolTable_append(localFunc->local_st, copySym);
            }
            walker = (walker != NULL) ? walker->nextSymbol : walker;
        }
        $$ = NULL;
    }
    ;

func_stmts:
    /* empty */ { $$ = NULL; }
    | stmt func_stmts {
        $$ = $1;
        $1->nextStmt = $2;
    }
    ;

stmt:
    IF_KEYWD '(' expr ')' stmt { 
        Node* node = Node_new(); node->op = IF_STMT_NODEOP;
        char buffer[128];
        if (!typesAreCompatible($3->resultType, BOOL_TYPE) && $3->resultType != ERROR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Expected if statement expression to have result type of boolean, got a type of [%s]", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        node->child1 = $3;
        node->child2 = $5;
        $$ = node;
    }
    | IF_KEYWD '(' expr ')' stmt ELSE_KEYWD stmt { 
        Node* node = Node_new(); node->op = IF_STMT_NODEOP;
        char buffer[128];
        if (!typesAreCompatible($3->resultType, BOOL_TYPE) && $3->resultType != ERROR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Expected if statement expression to have result type of boolean, got a type of [%s]", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        node->child1 = $3;
        node->child2 = $5;
        node->child3 = $7;
        $$ = node;
    }
    | WHILE_KEYWD '(' expr ')' stmt {  
        Node* node = Node_new(); node->op = WHILE_STMT_NODEOP;
        char buffer[128];
        if (!typesAreCompatible($3->resultType, BOOL_TYPE) && $3->resultType != ERROR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Expected while statement expression to have result type of boolean, got a type of [%s]", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        node->child1 = $3;
        node->child2 = $5;
        $$ = node;
    }
    | FOR_KEYWD '(' for_sub_1 ';' for_sub_2 ';' for_sub_3 ')' stmt { 
        Node* node = Node_new(); node->op = FOR_STMT_NODEOP;
        char buffer[128];
        /* TODO: Why is this NULL check needed? */
        if ($5 != NULL && (!typesAreCompatible($5->resultType, BOOL_TYPE) && $5->resultType != ERROR_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Expected for statement expression to have result type of boolean, got a type of [%s]", ($5 == NULL) ? SymbolType_toStr($5->resultType) : "NULL");
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        node->child1 = $3;
        node->child2 = $5;
        node->child3 = $7;
        node->child4 = $9;
        $$ = node;
    }
    | RETURN_KEYWD ';' { 
        Node* node = Node_new(); node->op = RETURN_NODEOP; node->resultType = VOID_TYPE; 
        if (localFunc == NULL) {
            yyerror("Invalid return statement not inside a function");
        }
        else if (localFunc->function_returnType != VOID_TYPE) {
            yyerror("Expected return value in function with non-void return type");
            node->resultType = ERROR_TYPE;
        } else {
            seenReturn = true;
        }
        $$ = node;
    }
    | RETURN_KEYWD expr ';' {
        seenReturn = true;
        Node* node = Node_new(); node->op = RETURN_NODEOP; node->resultType = UNASSIGNED_TYPE; /* TODO: I don't think we want this node->resultType = $2->resultType; */ 
        char buffer[256];
        if (localFunc == NULL) {
            yyerror("Invalid return statement not inside a function");
        }
        else if (!typesAreCompatible(localFunc->function_returnType, $2->resultType) && $2->resultType != ERROR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Unable to return expression of type [%s] when return type is [%s]", SymbolType_toStr($2->resultType), SymbolType_toStr(localFunc->function_returnType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else {
            seenReturn = true;
        }
        node->child1 = $2;
        $$ = node;
    }
    | assg ';' { $$ = $1; }
    | ID '(' ')' ';' {
        Node* node = Node_new(); node->op = FUNC_CALL_NODEOP;
        Symbol* symbol = SymbolTable_get(st, $1->name);
        char buffer[256];
        /* TODO: Maybe a memory leak with $1 here */
        if (symbol == NULL) {
            snprintf(buffer, sizeof(buffer), "Unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (symbol->type != FUNC_NODEF_TYPE && symbol->type != EXTERN_FUNC_NODEF_TYPE && symbol->type != FUNC_HASDEF_TYPE) {
            snprintf(buffer, sizeof(buffer), "Identifier is not a function [%s]", symbol->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } 
        else if (symbol->function_arg->type != VOID_TYPE) {
            snprintf(buffer, sizeof(buffer), "Expected argument of type [%s]", SymbolType_toStr(symbol->function_arg->type));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (symbol->function_returnType != VOID_TYPE) {
            snprintf(buffer, sizeof(buffer), "Function [%s] has a nonvoid return type and thus cannot be called as a standalone statement", symbol->name); 
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else {
            node->resultType = symbol->function_returnType;
        }
        node->symbol = symbol;
        $$ = node;
    }
    | ID '(' expr expr_rpt ')' ';' { 
        Symbol* symbol = SymbolTable_get(st, $1->name);
        Node* node = Node_new(); node->op = FUNC_CALL_NODEOP; node->resultType = (symbol != NULL) ? symbol->function_returnType : UNASSIGNED_TYPE;
        char buffer[256];
        /* TODO: Maybe a memory leak with $1 here */
        if (symbol == NULL) {
            snprintf(buffer, sizeof(buffer), "Unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (symbol->type != FUNC_NODEF_TYPE && symbol->type != EXTERN_FUNC_NODEF_TYPE && symbol->type != FUNC_HASDEF_TYPE) {
            snprintf(buffer, sizeof(buffer), "Identifier is not a function [%s]", symbol->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible(symbol->function_arg->type, $3->resultType)) {
            snprintf(buffer, sizeof(buffer), "Type of expression [%s] is not compatible with expected argument type of [%s]", SymbolType_toStr($3->resultType), SymbolType_toStr(symbol->function_arg->type));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (symbol->function_returnType != VOID_TYPE) {
            snprintf(buffer, sizeof(buffer), "Function [%s] has a nonvoid return type and thus cannot be called as a standalone statement", symbol->name); 
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } 
        else {
            Node* resultWalker = $4;
            Symbol* argWalker = symbol->function_arg->nextSymbol;
            while (argWalker != NULL && resultWalker != NULL) {
                if (!typesAreCompatible(argWalker->type, resultWalker->resultType)) {
                    snprintf(buffer, sizeof(buffer), "Type of expression [%s] is not compatible with expected argument type of [%s]", SymbolType_toStr(resultWalker->resultType), SymbolType_toStr(argWalker->type));
                    yyerror(buffer);
                    node->resultType = ERROR_TYPE;
                }
                argWalker = argWalker->nextSymbol;
                resultWalker = resultWalker->nextStmt;
            }
            if ((argWalker == NULL && resultWalker != NULL) || (argWalker != NULL && resultWalker == NULL)) {
                snprintf(buffer, sizeof(buffer), "Incorrect number of arguments to function [%s]", symbol->name);
                yyerror(buffer);
                node->resultType = ERROR_TYPE;
            }
        }
        node->symbol = symbol;
        node->child1 = $3;
        $3->nextStmt = $4;
        $$ = node;
    }
    | '{' '}' { Node* node = Node_new(); node->op = NOOP_NODEOP; $$ = node; }
    | '{' stmt stmt_rpt '}' { $$ = $2; /* TODO: Make sure that this is a valid way of concatenating statements */ $2->nextStmt = $3; }
    | ';' { Node* node = Node_new(); node->op = NOOP_NODEOP; $$ = node; }
    ;

for_sub_1:
    /* empty */ { $$ = NULL; /*Node* node = Node_new(); node->op = NOOP_NODEOP; node->resultType = UNASSIGNED_TYPE; $$ = node;*/ }
    | assg { $$ = $1; }
    ;

for_sub_2:
    /* empty */ { $$ = NULL; }
    | expr { $$ = $1; }
    ;

for_sub_3:
    /* empty */ { $$ = NULL; /*Node* node = Node_new(); node->op = NOOP_NODEOP; node->resultType = UNASSIGNED_TYPE;  $$ = node;*/ }
    | assg { $$ = $1; }
    ;

expr_rpt:
    /* empty */ { $$ = NULL; }
    | ',' expr expr_rpt { $$ = $2; $2->nextStmt = $3; /* TODO: This might be some tech debt for code gen */ }
    ;

stmt_rpt:
    /* empty */ { $$ = NULL; /*Node* node = Node_new(); node->op = NOOP_NODEOP; $$ = node;*/ }
    | stmt stmt_rpt { $1->nextStmt = $2; /* TODO: Is this good? */ $$ = $1; }
    ;

assg:
    ID '=' expr {
        Node* root = Node_new();
        Node* left = Node_new();
        
        Symbol* idSym = SymbolTable_get(st, $1->name);
        //fprintf(stderr, "Performing assignment to variable [%s]\n", $1->name);
        char buffer[256];
        /* We want the local function arg to replace this arg if it overrides a global one */
        //fprintf(stderr, "The function I'm in has an argument named [%s] of type [%s]\n", localFunc->function_arg->name, SymbolType_toStr(localFunc->function_arg->type));
        if (localFunc != NULL) {
            Symbol* idSym2 = SymbolTable_get(localFunc->local_st, $1->name);
            if (idSym2 != NULL) {
                idSym = idSym2;
                //fprintf(stderr, "Hey look it's actually been redefined as a function variable\n");
            }
        }
        /* Probably Symbol_free($1) now but since memory usage isn't a priority atm... (I'm sorry valgrind) */
        /* Check that ID has been declared */
        if (idSym == NULL) {
            snprintf(buffer, sizeof(buffer), "Assignment to unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        }
        /* Check that ID isn't an array type */
        else if (idSym->type == CHARARR_TYPE || idSym->type == INTARR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Unable to perform assignment operation to array [%s]. Must be array", idSym->name);
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        }
        /* Check that the type of expr matches the destination identifier */
        else if ($3 != NULL && $3->resultType != ERROR_TYPE && !typesAreCompatible(idSym->type, $3->resultType)) {
            snprintf(buffer, sizeof(buffer), "Unable to assign result of type [%s] to variable of type [%s]", SymbolType_toStr($3->resultType), SymbolType_toStr(idSym->type));
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        } 
        else {
            left->resultType = idSym->type;
            root->resultType = idSym->type;
        }
        left->op = VAR_NODEOP;
        left->symbol = idSym;
        root->op = ASSG_NODEOP;
        root->child1 = left;
        root->child2 = $3;
        $$ = root;
        //fprintf(stderr, "Performing assignment to var [%s] of type [%s]\n", idSym->name, SymbolType_toStr(idSym->type));
    }
    | ID '[' expr ']' '=' expr {
        /* check that $3 is an integer expression and $1's type matches $3. Unneeded for part 1 */
        Node* root = Node_new();
        Node* left = Node_new();
        Node* leftleft = Node_new();

        Symbol* idSym = SymbolTable_get(st, $1->name);
        char buffer[256];
        if (localFunc != NULL) {
            Symbol* idSym2 = SymbolTable_get(localFunc->local_st, $1->name);
            if (idSym2 != NULL) {
                idSym = idSym2;
            }
        }
        /* Check that ID has been declared */
        if (idSym == NULL) {
            snprintf(buffer, sizeof(buffer), "Assignment to unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        }
        /* Check that ID is an array type. Can't be just a arr/nonarr type since full declaration must have occurred earlier */
        else if (idSym->type != CHARARR_TYPE && idSym->type != INTARR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Identifier [%s] is not of type int[] or char[]", idSym->name);
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        }
        /* Check that indexing expr is compatible with int resultType */
        else if ($3 == NULL || !typesAreCompatible($3->resultType, INT_TYPE)) {
            if ($3 == NULL) {
                snprintf(buffer, sizeof(buffer), "Unable to index into array with null expression pointer");
            } else {
                snprintf(buffer, sizeof(buffer), "Unable to index into array with expression of resultant type [%s]", SymbolType_toStr($3->resultType));
            }
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        }
        /* Check that the type of second expr is compatible with the type of this indexed expression */
        else if ($6 == NULL || !typesAreCompatible(getTypeForArrType(idSym->type), $6->resultType)) {
            if ($6 == NULL) {
                snprintf(buffer, sizeof(buffer), "Unable to perform assignment when right hand side is null expression pointer");
            } else {
                snprintf(buffer, sizeof(buffer), "Unable to assign result of type [%s] to array with elements of type [%s]", SymbolType_toStr($6->resultType), SymbolType_toStr(getTypeForArrType(idSym->type)));
            }
            yyerror(buffer);
            root->resultType = ERROR_TYPE;
        }
        leftleft->op = VAR_NODEOP;
        leftleft->symbol = idSym;
        left->op = ARRAY_INDEX_NODEOP;
        left->child1 = leftleft;
        left->child2 = $3;
        root->op = ASSG_NODEOP;
        root->child1 = left;
        root->child2 = $6;
        $$ = root;
    }
    ;

expr:
    '-' expr %prec UMINUS { 
        Node* node = Node_new(); node->op = MINUS_UNARY_NODEOP; node->child1 = $2; node->resultType = INT_TYPE;
        char buffer[128]; 
        if ($2->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($2->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Expression with resultant type of [%s] is not compatible with the unary minus operator", SymbolType_toStr($2->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node; 
    }
    | '!' expr { 
        Node* node = Node_new(); node->op = NOT_LOGIC_NODEOP; node->child1 = $2; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($2->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($2->resultType, BOOL_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Expression with resultant type of [%s] is not compatible with boolean negation operator", SymbolType_toStr($2->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr '+' expr { 
        Node* node = Node_new(); node->op = PLUS_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = INT_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with + operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with + operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr '-' expr { 
        Node* node = Node_new(); node->op = MINUS_BINARY_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = INT_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with - operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with - operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr '*' expr { 
        Node* node = Node_new(); node->op = MULT_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = INT_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with * operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with * operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node; 
    }
    | expr '/' expr { 
        Node* node = Node_new(); node->op = DIVIDE_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = INT_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with / operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with / operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node; 
    }
    | expr EQ_RELOP expr { 
        Node* node = Node_new(); node->op = EQ_REL_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with == operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with == operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr NEQ_RELOP expr{
        Node* node = Node_new(); node->op = NEQ_REL_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with != operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with != operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr LTE_RELOP expr {
        Node* node = Node_new(); node->op = LTE_REL_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with <= operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with <= operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr LT_RELOP expr {
        Node* node = Node_new(); node->op = LT_REL_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with < operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with < operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr GTE_RELOP expr {
        Node* node = Node_new(); node->op = GTE_REL_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with >= operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with >= operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr GT_RELOP expr {
        Node* node = Node_new(); node->op = GT_REL_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with > operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with > operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr AND_LOGICOP expr {
        Node* node = Node_new(); node->op = AND_LOGIC_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, BOOL_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with && operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, BOOL_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with && operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | expr OR_LOGICOP expr {
        Node* node = Node_new(); node->op = OR_LOGIC_NODEOP; node->child1 = $1; node->child2 = $3; node->resultType = BOOL_TYPE;
        char buffer[128];
        if ($1->resultType == ERROR_TYPE || $3->resultType == ERROR_TYPE) {
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($1->resultType, BOOL_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Left operand with resultant type [%s] is incompatible with || operator", SymbolType_toStr($1->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, BOOL_TYPE)) {
            snprintf(buffer, sizeof(buffer), "Right operand with resultant type [%s] is incompatible with || operator", SymbolType_toStr($3->resultType));
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        $$ = node;
    }
    | ID { 
        Node* node = Node_new(); node->op = VAR_NODEOP;
        Symbol* idSym = SymbolTable_get(st, $1->name);
        char buffer[256];
        if (localFunc != NULL) {
            Symbol* idSym2 = SymbolTable_get(localFunc->local_st, $1->name);
            if (idSym2 != NULL) {
                idSym = idSym2;
            }
        }
        if (idSym == NULL) {
            snprintf(buffer, sizeof(buffer), "Usage of unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else {
            node->resultType = idSym->type;
        }
        node->symbol = idSym; $$ = node; 
    }
    | ID '(' ')' { 
        Node* node = Node_new(); node->op = FUNC_CALL_NODEOP;
        Symbol* idSym = SymbolTable_get(st, $1->name); 
        char buffer[256];
        if (idSym == NULL) {
            snprintf(buffer, sizeof(buffer), "Unable to call unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else if (idSym->type != FUNC_HASDEF_TYPE && idSym->type != FUNC_NODEF_TYPE && idSym->type != EXTERN_FUNC_NODEF_TYPE) {
            snprintf(buffer, sizeof(buffer), "Identifier is not a function: [%s]", idSym->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else if (idSym->function_arg->type != VOID_TYPE) {
            snprintf(buffer, sizeof(buffer), "Expected argument of type [%s] while calling function [%s]", SymbolType_toStr(idSym->function_arg->type), idSym->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else {
            node->resultType = idSym->function_returnType;
        }
        node->symbol = idSym; $$ = node; 
    }
    | ID '(' expr expr_rpt ')' { 
        /* TODO: Merge the subexpressions from expr_rpt into the tree */
        Node* node = Node_new(); node->op = FUNC_CALL_NODEOP; 
        Symbol* idSym = SymbolTable_get(st, $1->name);
        char buffer[256];
        if (idSym == NULL) {
            snprintf(buffer, sizeof(buffer), "Unable to call unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else if (idSym->type != FUNC_HASDEF_TYPE && idSym->type != FUNC_NODEF_TYPE && idSym->type != EXTERN_FUNC_NODEF_TYPE) {
            snprintf(buffer, sizeof(buffer), "Identifier is not a function: [%s]", idSym->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } else if (!typesAreCompatible(idSym->function_arg->type, $3->resultType)) {
            if ($3->resultType != ERROR_TYPE) {
                snprintf(buffer, sizeof(buffer), "Expected argument of type [%s] while calling function [%s]", SymbolType_toStr(idSym->function_arg->type), idSym->name);
                yyerror(buffer);
            }
            node->resultType = ERROR_TYPE;
        } else {
            Node* resultWalker = $4;
            Symbol* argWalker = idSym->function_arg->nextSymbol;
            while (argWalker != NULL && resultWalker != NULL) {
                if (!typesAreCompatible(argWalker->type, resultWalker->resultType)) {
                    snprintf(buffer, sizeof(buffer), "Type of expression [%s] is not compatible with expected argument type of [%s]", SymbolType_toStr(resultWalker->resultType), SymbolType_toStr(argWalker->type));
                    yyerror(buffer);
                    node->resultType = ERROR_TYPE;
                }
                argWalker = argWalker->nextSymbol;
                resultWalker = resultWalker->nextStmt;
            }
            if ((argWalker == NULL && resultWalker != NULL) || (argWalker != NULL && resultWalker == NULL)) {
                snprintf(buffer, sizeof(buffer), "Incorrect number of arguments to function [%s]", idSym->name);
                yyerror(buffer);
                node->resultType = ERROR_TYPE;
            }
        }
        if (node->resultType != ERROR_TYPE) {
            node->resultType = idSym->function_returnType;
        }
        $3->nextStmt = $4;
        node->symbol = idSym; node->child1 = $3; $$ = node; 
    }
    | ID '[' expr ']' {
        Node* node = Node_new(); node->op = ARRAY_INDEX_NODEOP;
        Node* left = Node_new(); left->op = VAR_NODEOP;
        Symbol* idSym = SymbolTable_get(st, $1->name);
        char buffer[256];
        if (localFunc != NULL) {
            Symbol* idSym2 = SymbolTable_get(localFunc->local_st, $1->name);
            if (idSym2 != NULL) {
                idSym = idSym2;
            }
        }
        if (idSym == NULL) {
            snprintf(buffer, sizeof(buffer), "Usage of unknown identifier: [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        } 
        else if (idSym->type != INTARR_TYPE && idSym->type != CHARARR_TYPE) {
            snprintf(buffer, sizeof(buffer), "Unable to index into nonarray identifier [%s]", $1->name);
            yyerror(buffer);
            node->resultType = ERROR_TYPE;
        }
        else if (!typesAreCompatible($3->resultType, INT_TYPE) && $3->resultType != ERROR_TYPE) {
            yyerror("Unable to index array with noninteger expression");
            node->resultType = ERROR_TYPE;
        } else {
            node->resultType = getTypeForArrType(idSym->type);
        }
        left->symbol = idSym;
        node->child1 = left; node->child2 = $3; $$ = node;
    }
    | '(' expr ')'  { $$ = $2; }
    | INTCON    { Node* node = Node_new(); node->op = CONST_NODEOP; node->intVal = $1; node->resultType = INT_TYPE; $$ = node; }
    | CHARCON   { Node* node = Node_new(); node->op = CONST_NODEOP; node->charVal = $1; node->resultType = CHAR_TYPE; $$ = node; }
    | STRINGCON { 
        Node* node = Node_new(); 
        node->op = CONST_NODEOP; 
        StringTable* strVal = StringTable_new($1);
        node->stringVal = strVal;
        strTab = StringTable_append(strTab, strVal);
        node->resultType = CHARARR_TYPE; 
        $$ = node; 
    }
    | error { Node* node = Node_new(); node->resultType = ERROR_TYPE; $$ = node; }
    ;

%%

SymbolType getArrTypeForType(SymbolType type) {
    switch(type) {
        case INT_TYPE:
        case INTARR_TYPE:
        return INTARR_TYPE;
        case CHAR_TYPE:
        case CHARARR_TYPE:
        return CHARARR_TYPE;
        default:
        return type;
    }
}

SymbolType getTypeForArrType(SymbolType type) {
    switch(type) {
        case INT_TYPE:
        case INTARR_TYPE:
        return INT_TYPE;
        case CHAR_TYPE:
        case CHARARR_TYPE:
        return CHAR_TYPE;
        default:
        return type;
    }
}

bool isInvalidSymbolDecl(Symbol* root, Symbol* symbol) {
    if (SymbolTable_contains(root, symbol->name)) {
        char buffer[256];
        snprintf(buffer, sizeof(buffer), "Found repeated declaration of identifier: [%s]", symbol->name);
        yyerror(buffer);
        return true;
    }
    return false;
}

bool isInvalidFuncDecl(Symbol* root, Symbol* func) {
    Symbol* prevDecl = SymbolTable_get(root, func->name);
    char buffer[256];
    if (prevDecl == NULL) {
        return false;
    }
    /* So a previous definition exists... */
    if (prevDecl->type == FUNC_HASDEF_TYPE) {
        snprintf(buffer, sizeof(buffer), "Function [%s] has already been defined", func->name);
        yyerror(buffer);
        return true;
    }
    if (prevDecl->type == EXTERN_FUNC_NODEF_TYPE) {
        snprintf(buffer, sizeof(buffer), "Definition of extern function [%s] must not be in the same file where it is declared as extern", func->name);
        yyerror(buffer);
        return true;
    }
    /* So the previous symbol was just a declaration */
    if (func->type != FUNC_HASDEF_TYPE) {
        snprintf(buffer, sizeof(buffer), "Found repeated declaration of function identifier: [%s]. Only one prototype may be defined", func->name);
        yyerror(buffer);
        return true;
    }
    if (prevDecl->function_returnType != func->function_returnType) {
        snprintf(buffer, sizeof(buffer), "Found definition of prototyped function identifier and return type doesn't match: [%s]", func->name);
        yyerror(buffer);
        return true;
    }
    Symbol* funcPtr = func->function_arg;
    Symbol* prevDeclPtr = prevDecl->function_arg;
    while (funcPtr != NULL && prevDeclPtr != NULL) {
        if (prevDeclPtr->type != funcPtr->type) {
            snprintf(buffer, sizeof(buffer), "Found definition of prototype function identifier and args don't match: [%s]", func->name);
            yyerror(buffer);
            return true;
        }
        funcPtr = funcPtr->nextSymbol;
        prevDeclPtr = prevDeclPtr->nextSymbol;
    }
    if ((funcPtr == NULL && prevDeclPtr != NULL) || (funcPtr != NULL && prevDeclPtr == NULL)) {
        snprintf(buffer, sizeof(buffer), "Found definition of prototype function and number of arguments don't match: [%s]", func->name);
        yyerror(buffer);
        return true;
    }
    return false;
}

bool typesAreCompatible(SymbolType type1, SymbolType type2) {
    if (type1 == type2) {
        return true;
    }
    else if (type1 == INT_TYPE && type2 == CHAR_TYPE) {
        return true;
    }
    else if (type1 == CHAR_TYPE && type2 == INT_TYPE) {
        return true;
    }
    return false;
}

void yyerror(const char *s) {
    fprintf(stderr, "Line %d: %s\n", yylineno, s);
    encounteredParsingError = 1;
}