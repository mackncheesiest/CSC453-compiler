/* 
	Use ranges to recreate [[:print:]] while excluding single quote and backslash 
	See table at https://en.cppreference.com/w/cpp/string/byte/isprint
*/

%{
#include "c--parserspec.tab.h"
#include <string.h>
#include <stdlib.h>
int inInitialState = 1;
%}
%option yylineno

/* between ' ' and '&' OR between '(' and '[' OR between ']' and '~' -- removing single quote and backslash */
charcon_isprint   ([ -&]|[(-[]|[]-~])
/* between ' ' and '!' OR between '#' and '~' -- removing double quote */
strcon_isprint    ([ -!]|[#-~])

letter      ([a-z]|[A-Z])
digit       ([0-9])
charcon     \'({charcon_isprint}|\\n|\\0)\'
stringcon   \"({strcon_isprint})*\"

%x LEXING_COMMENT

%%

"/*"                                { BEGIN(LEXING_COMMENT); inInitialState = 0; }
<LEXING_COMMENT>"*/"                { BEGIN(INITIAL); inInitialState = 1; }
<LEXING_COMMENT>.                   { }
<LEXING_COMMENT>\n                  { }

"int"                               { yylval.typeEnum = INT_TYPE; return INT_KEYWD; }
"char"                              { yylval.typeEnum = CHAR_TYPE; return CHAR_KEYWD; }
"extern"                            { return EXTERN_KEYWD; }
"void"                              { yylval.typeEnum = VOID_TYPE; return VOID_KEYWD; }
"if"                                { return IF_KEYWD; }
"else"                              { return ELSE_KEYWD; }
"for"                               { return FOR_KEYWD; }
"while"                             { return WHILE_KEYWD; }
"return"                            { return RETURN_KEYWD; }

"("                                 { return '('; }
")"                                 { return ')'; }
"{"                                 { return '{'; }
"}"                                 { return '}'; }
"["                                 { return '['; }
"]"                                 { return ']'; }
";"                                 { return ';'; }
","                                 { return ','; }

"="                                 { return '='; }

"+"                                 { return '+'; }
"-"                                 { return '-'; }
"*"                                 { return '*'; }
"/"                                 { return '/'; }
"!"									{ return '!'; }

"=="                                { return EQ_RELOP; }
"!="                                { return NEQ_RELOP; }
"<="                                { return LTE_RELOP; }
"<"                                 { return LT_RELOP; }
">="                                { return GTE_RELOP; }
">"                                 { return GT_RELOP; }

"&&"                                { return AND_LOGICOP; }
"||"                                { return OR_LOGICOP; }


{stringcon}                         { 
	if (NULL != ((yylval.stringVal) = (char*)calloc(strlen(yytext)+1, 1))) {
		strcpy(yylval.stringVal, yytext);
	}
	return STRINGCON; 
}
{letter}({letter}|{digit}|_)*       { 
	// fprintf(stderr, "[lexer] Found identifier: [%s]\n", yytext); 
	//TODO: This may be introducing memory leaks. Might need to use some bison %destructor's to properly free the memory back
	Symbol* sym = Symbol_new(yytext, UNASSIGNED_TYPE);
	yylval.symbolPtr = sym;
	return ID;
}
{digit}+                            { yylval.intVal = atoi(yytext); return INTCON; }
[[:space:]]+                        { }
{charcon}                           {
	if (strcmp(yytext, "'\\n'") == 0) {
		yylval.charVal = '\n';
	} else if (strcmp(yytext, "'\\0'") == 0) {
		yylval.charVal = '\0';
	} else {
		yylval.charVal = yytext[1];
	}
	return CHARCON;
}

<<EOF>>								{ BEGIN(INITIAL); return 0; /* Intentionally not setting inInitialState here to allow the parser to YYABORT if necessary */ }

.									{ /* fprintf(stdout, "[lexer] Line %d, found this text that didn't match anything: [%s]\n", yylineno, yytext); */ return yytext[0]; }

%%

int yywrap(void) { return 1; }
