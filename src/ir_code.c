#include "ir_code.h"

extern void yyerror(const char *s);

static int tmpCounter = 0;
static int labelCounter = 0;

void initializeOutput(FILE* outputFile) {
    //Do this at the beginning of the compilation process since, because we process one function definition at a time, we lack the context of what the _first_ function definition is
    fprintf(outputFile, ".text\n");
    fprintf(outputFile, ".globl main\n\n");
    fprintf(outputFile, "main:\n");
    /* Enter main */
    fprintf(outputFile, "addi $sp, $sp, -8\n");
    fprintf(outputFile, "sw $ra, 4($sp)\n");
    fprintf(outputFile, "sw $fp, 0($sp)\n");
    fprintf(outputFile, "add $fp, $0, $sp\n");
    /* Goto real main */
    fprintf(outputFile, "jal _main\n");
    /* Return from main */
    fprintf(outputFile, "add $sp, $0, $fp\n");
    fprintf(outputFile, "lw $ra, 4($sp)\n");
    fprintf(outputFile, "lw $fp, 0($sp)\n");
    fprintf(outputFile, "addi $sp, $sp, 8\n");
    fprintf(outputFile, "jr $ra\n\n");

    fprintf(outputFile, "_print_int:\n");
    fprintf(outputFile, "li $v0, 1\n");
    fprintf(outputFile, "lw $a0, 0($sp)\n");
    fprintf(outputFile, "syscall\n");
    fprintf(outputFile, "jr $ra\n\n");

    fprintf(outputFile, "_print_string:\n");
    fprintf(outputFile, "li $v0, 4\n");
    fprintf(outputFile, "lw $a0, 0($sp)\n");
    fprintf(outputFile, "syscall\n");
    fprintf(outputFile, "jr $ra\n\n");
}

void generateFunction(Node* syntaxTree, Symbol* global_st, Symbol* localFunc, FILE* outputFile) {
    fprintf(outputFile, "########################## Generating Code for Function %s ##############################\n", localFunc->name);
    if (syntaxTree == NULL) {
        fprintf(outputFile, "_%s:\n", localFunc->name);
        fprintf(outputFile, "#Empty syntax tree, returning to caller\n");
        fprintf(outputFile, "jr $ra\n\n");
        return;
    }
    // Tree_print(syntaxTree, 0);
    IR_Instr* head = IR_Instr_new(ENTER_IROP, localFunc, NULL, NULL, NULL, NULL);

    generateIR(syntaxTree, localFunc, false);

    head = IR_Instr_append(head, syntaxTree->irCode);
    head = IR_Instr_append(head, IR_Instr_new(LEAVE_IROP, localFunc, NULL, NULL, NULL, NULL));
    head = IR_Instr_append(head, IR_Instr_new(RETURN_IROP, NULL, NULL, NULL, NULL, NULL));

    assignSymbolOffsets(localFunc);

    // fprintf(outputFile, "Final tree before MIPS generation:\n");
    // Tree_print(syntaxTree, 0);
    // IR_InstrList_print(head, stdout);
    IR_Instr* walker = head;
    IR_Instr* prev = walker;
    while (walker != NULL) {
        printIRToMIPS(walker, outputFile);
        prev = walker;
        walker = walker->nextInstr;
        IR_Instr_free(prev);
    }
    /* Do a little dance ┏(･o･)┛♪┗ (･o･) ┓ */
}

void generateGlobalCode(Symbol* global_st, StringTable* strTab, FILE* outputFile) {
    char buffer[128]; 
    int i;
    fprintf(outputFile, ".data\n");
    Symbol* walker = global_st;
    while (walker != NULL) {
        switch (walker->type) {
            case INT_TYPE:
                fprintf(outputFile, "_%s: .word 0\n", walker->name);
                break;
            case CHAR_TYPE:
                fprintf(outputFile, "_%s: .byte 0\n", walker->name);
                break;
            case INTARR_TYPE:
                if (walker->array_numElements == 0) {
                    break;
                }
                fprintf(outputFile, "_%s: .word 0", walker->name);
                for (i = 1; i < walker->array_numElements; i++) {
                    fprintf(outputFile, ", 0");
                }
                fprintf(outputFile, "\n");
                break;
            case CHARARR_TYPE:
                if (walker->array_numElements == 0) {
                    /* TODO: Is this behavior okay? */
                    break;
                }
                fprintf(outputFile, "_%s: .byte 0", walker->name);
                for (i = 1; i < walker->array_numElements; i++) {
                    fprintf(outputFile, ", 0");
                }
                fprintf(outputFile, "\n");
                break;
            case FUNC_NODEF_TYPE:
            case EXTERN_FUNC_NODEF_TYPE:
            case FUNC_HASDEF_TYPE:
            case HEAD_TYPE:
                /* noop */
                break;
            default:
            snprintf(buffer, sizeof(buffer), "Unable allocate global data partition for symbol of type %s", SymbolType_toStr(walker->type));
            yyerror(buffer);
            break;
        }
        walker = walker->nextSymbol;
    }
    StringTable* walker2 = strTab;
    while (walker2 != NULL) {
        fprintf(outputFile, "_%s: .asciiz %s\n", walker2->identifier, walker2->strVal);
        walker2 = walker2->nextEntry;
    }
    fprintf(outputFile, "\n");
}

IR_Instr* generateIR(Node* syntaxTree, Symbol* functionSym, bool isLValue) {
    char buffer[128];
    if (syntaxTree == NULL) {
        return NULL;
    }
    if (syntaxTree->resultType == ERROR_TYPE) {
        //Skip code generation for functions that had ERROR_TYPE bubble up to the top
        return generateIR(syntaxTree->nextStmt, functionSym, false);
    }
    else if (syntaxTree->op == IF_STMT_NODEOP || syntaxTree->op == FOR_STMT_NODEOP || syntaxTree->op == WHILE_STMT_NODEOP) {
        generateControlFlowIR(syntaxTree, functionSym, NULL, NULL);
        syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->nextStmt, functionSym, false));
        return syntaxTree->irCode;
    }
    else if (syntaxTree->op == ASSG_NODEOP) {
        syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child1, functionSym, true));
        syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child2, functionSym, false));
        Symbol* src1 = syntaxTree->child2->destSymbol;
        Symbol* dest = syntaxTree->child1->destSymbol;
        syntaxTree->destSymbol = dest;
        /* TODO: This is disgusting */
        if (syntaxTree->child1->op == ARRAY_INDEX_NODEOP) {
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, IR_Instr_new(MEM_ASSG_IROP, src1, NULL, dest, NULL, NULL));
        } else {
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, IR_Instr_new(ASSG_IROP, src1, NULL, dest, NULL, NULL));
        }
        syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->nextStmt, functionSym, false));
        return syntaxTree->irCode;
    }

    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child1, functionSym, false));
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child2, functionSym, false));
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child3, functionSym, false));
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child4, functionSym, false));

    // Do thing for this node
    IR_Instr* currInstr = NULL;
    IR_Instr* walkerInstr = NULL;
    Symbol *src1 = NULL, *src2 = NULL, *dest = NULL;
    Symbol *arrayIndex, *finalArrayLoc = NULL, *arrayStVal = NULL, *arrayAccResult = NULL; 
    IR_LitVal litval;
    Node* argWalker;
    int typeSize;
    switch (syntaxTree->op) {
        case UNASSIGNED_NODEOP:
            yyerror("Unable to generate IR code for tree node with unassigned node operation");
            break;

        case NOOP_NODEOP:
            /* noop */
            break;

        case CONST_NODEOP:
            dest = getTemp(syntaxTree->resultType, functionSym->local_st);
            // fprintf(stdout, "I just instantiated a great new temp named %s\n", dest->name);
            syntaxTree->destSymbol = dest;
            currInstr = IR_Instr_new(CONST_IROP, NULL, NULL, dest, NULL, NULL);
            switch (syntaxTree->resultType) {
                case INT_TYPE:
                    litval.i = syntaxTree->intVal;
                    break;
                case CHAR_TYPE:
                    litval.c = syntaxTree->charVal;
                    break;
                case CHARARR_TYPE:
                    // fprintf(stdout, "My string identifier is (%s)\n", syntaxTree->stringVal->identifier);
                    // fprintf(stdout, "My string val is (%s)\n", syntaxTree->stringVal->strVal);
                    litval.s = syntaxTree->stringVal;
                    dest->codegen_isPtr = true;
                    break;
            }
            currInstr->val = litval;
            break;

        case VAR_NODEOP:
            syntaxTree->destSymbol = syntaxTree->symbol;
            break;

        case FUNC_CALL_NODEOP:
            argWalker = syntaxTree->child1;
            currInstr = useRecursionToBuildTheParamInstructionsInReverse(currInstr, argWalker);
            currInstr = IR_Instr_append(currInstr, IR_Instr_new(CALL_IROP, syntaxTree->symbol, NULL, NULL, NULL, NULL));
            if (syntaxTree->resultType != VOID_TYPE) {
                currInstr = IR_Instr_append(
                    currInstr, 
                    IR_Instr_new(RETRIEVE_IROP, NULL, NULL, getTemp(syntaxTree->resultType, functionSym->local_st), NULL, NULL)
                );
            }
            walkerInstr = currInstr;
            while (walkerInstr->nextInstr != NULL) {
                walkerInstr = walkerInstr->nextInstr;
            }
            /* TODO: Need to better deal with single tree nodes generating multiple IR instructions. Maybe? */
            syntaxTree->destSymbol = walkerInstr->dest;
            break;

        case ARRAY_INDEX_NODEOP:
            typeSize = syntaxTree->child1->symbol->type == CHARARR_TYPE ? 1 : 4;

            arrayIndex = getTemp(INT_TYPE, functionSym->local_st);
            currInstr = IR_Instr_new(CONST_IROP, NULL, NULL, arrayIndex, NULL, NULL);
            litval.i = typeSize;
            currInstr->val = litval;

            currInstr = IR_Instr_append(currInstr, IR_Instr_new(MULT_BINARY_IROP, arrayIndex, syntaxTree->child2->destSymbol, arrayIndex, NULL, NULL));
            
            finalArrayLoc = getTemp(ADDR_TYPE, functionSym->local_st);
            arrayStVal = getTemp(syntaxTree->child1->symbol->type, functionSym->local_st);
            currInstr = IR_Instr_append(currInstr, IR_Instr_new(ADDR_OF_IROP, syntaxTree->child1->destSymbol, NULL, finalArrayLoc, NULL, NULL));
            currInstr = IR_Instr_append(currInstr, IR_Instr_new(PLUS_BINARY_IROP, finalArrayLoc, arrayIndex, arrayStVal, NULL, NULL));
            if (isLValue) {
                syntaxTree->destSymbol = arrayStVal;
                // syntaxTree->destSymbol->type = syntaxTree->child1->symbol->type;
                syntaxTree->destSymbol->codegen_isPtr = true;
            } else {
                /* TODO: Port getTypeForArrType to a more globally accessible area */
                arrayAccResult = getTemp(syntaxTree->child1->symbol->type == CHARARR_TYPE ? CHAR_TYPE : INT_TYPE, functionSym->local_st);
                currInstr = IR_Instr_append(currInstr, IR_Instr_new(MEM_DEREF_IROP, arrayStVal, NULL, arrayAccResult, NULL, NULL));
                syntaxTree->destSymbol = arrayAccResult;
            }
            break;
            
        case MINUS_UNARY_NODEOP:
            src1 = syntaxTree->child1->destSymbol;
            dest = getTemp(INT_TYPE, functionSym->local_st);
            syntaxTree->destSymbol = dest;
            currInstr = IR_Instr_new(MINUS_UNARY_IROP, src1, NULL, dest, NULL, NULL);
            break;

        case RETURN_NODEOP:
            src1 = (syntaxTree->child1 != NULL) ? syntaxTree->child1->destSymbol : NULL;
            currInstr = IR_Instr_new(LEAVE_IROP, functionSym, NULL, NULL, NULL, NULL);
            currInstr = IR_Instr_append(currInstr, IR_Instr_new(RETURN_IROP, src1, NULL, NULL, NULL, NULL));
            break;

        case PLUS_NODEOP:
            src1 = syntaxTree->child1->destSymbol;
            src2 = syntaxTree->child2->destSymbol;
            dest = getTemp(INT_TYPE, functionSym->local_st);
            syntaxTree->destSymbol = dest;
            currInstr = IR_Instr_new(PLUS_BINARY_IROP, src1, src2, dest, NULL, NULL);
            break;

        case MINUS_BINARY_NODEOP:
            src1 = syntaxTree->child1->destSymbol;
            src2 = syntaxTree->child2->destSymbol;
            dest = getTemp(INT_TYPE, functionSym->local_st);
            syntaxTree->destSymbol = dest;
            currInstr = IR_Instr_new(MINUS_BINARY_IROP, src1, src2, dest, NULL, NULL);
            break;

        case MULT_NODEOP:
            src1 = syntaxTree->child1->destSymbol;
            src2 = syntaxTree->child2->destSymbol;
            dest = getTemp(INT_TYPE, functionSym->local_st);
            syntaxTree->destSymbol = dest;
            currInstr = IR_Instr_new(MULT_BINARY_IROP, src1, src2, dest, NULL, NULL);
            break;

        case DIVIDE_NODEOP:
            src1 = syntaxTree->child1->destSymbol;
            src2 = syntaxTree->child2->destSymbol;
            dest = getTemp(INT_TYPE, functionSym->local_st);
            syntaxTree->destSymbol = dest;
            currInstr = IR_Instr_new(DIVIDE_BINARY_IROP, src1, src2, dest, NULL, NULL);
            break;

        default:
            yyerror("Unable to generate IR code for tree node with unknown operation");
            break;
    }
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, currInstr);
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->nextStmt, functionSym, false));
    return syntaxTree->irCode;
}

IR_Instr* generateControlFlowIR(Node* syntaxTree, Symbol* localFunc, IR_Instr* trueDest, IR_Instr* falseDest) {
    if (syntaxTree == NULL) {
        return NULL;
    }
    /* If statement stuff */
    IR_Instr *thenLabel, *elseLabel, *afterLabel, *ifExpr, *thenBody, *elseBody, *gotoAfter;
    /* For loop stuff */
    IR_Instr *initAssg, *endAssg;
    /* While loop stuff */
    IR_Instr *topLabel, *evalLabel;
    /* AND/OR stuff */
    IR_Instr *rightLabel;

    switch(syntaxTree->op) {
        case IF_STMT_NODEOP:
            thenLabel = getLabel();
            elseLabel = getLabel();
            afterLabel = (syntaxTree->child3 != NULL) ? getLabel() : elseLabel;

            ifExpr = generateControlFlowIR(syntaxTree->child1, localFunc, thenLabel, elseLabel);
            thenBody = generateIR(syntaxTree->child2, localFunc, false);
            elseBody = generateIR(syntaxTree->child3, localFunc, false);

            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, ifExpr);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, thenLabel);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, thenBody);
            
            if (syntaxTree->child3 != NULL) {
                gotoAfter = IR_Instr_new(GOTO_IROP, NULL, NULL, NULL, afterLabel, NULL);
                syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, gotoAfter);
                syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, elseLabel);
                syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, elseBody);
            }
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, afterLabel);
            return syntaxTree->irCode;
        case FOR_STMT_NODEOP:
            topLabel = getLabel();
            evalLabel = getLabel();
            /* TODO: Figure out how to fully eliminate the after label even when a unary-not swaps NULL to be in TrueDest */
            afterLabel = getLabel();

            initAssg = generateIR(syntaxTree->child1, localFunc, false);
            ifExpr = generateControlFlowIR(syntaxTree->child2, localFunc, topLabel, afterLabel);
            endAssg = generateIR(syntaxTree->child3, localFunc, false);
            thenBody = generateIR(syntaxTree->child4, localFunc, false);

            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, initAssg);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(GOTO_IROP, NULL, NULL, NULL, evalLabel, NULL)
            );
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, topLabel);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, thenBody);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, endAssg);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, evalLabel);
            if (ifExpr == NULL) {
                syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                    IR_Instr_new(GOTO_IROP, NULL, NULL, NULL, topLabel, NULL)
                );
            } else {
                syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, ifExpr);
                syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, afterLabel);
            }
            return syntaxTree->irCode;
        case WHILE_STMT_NODEOP:
            topLabel = getLabel();
            evalLabel = getLabel();
            /* TODO: Figure out how to fully eliminate the after label even when a unary-not swaps NULL to be in TrueDest */
            afterLabel = getLabel();

            ifExpr = generateControlFlowIR(syntaxTree->child1, localFunc, topLabel, afterLabel);
            thenBody = generateIR(syntaxTree->child2, localFunc, false);

            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(GOTO_IROP, NULL, NULL, NULL, evalLabel, NULL)
            );
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, topLabel);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, thenBody);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, evalLabel);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, ifExpr);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, afterLabel);
            return syntaxTree->irCode;
        case AND_LOGIC_NODEOP:
            rightLabel = getLabel();
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode,
                generateControlFlowIR(syntaxTree->child1, localFunc, rightLabel, falseDest)
            );
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, rightLabel);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode,
                generateControlFlowIR(syntaxTree->child2, localFunc, trueDest, falseDest)
            );
            return syntaxTree->irCode;
        case OR_LOGIC_NODEOP:
            rightLabel = getLabel();
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode,
                generateControlFlowIR(syntaxTree->child1, localFunc, trueDest, rightLabel)
            );
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, rightLabel);
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode,
                generateControlFlowIR(syntaxTree->child2, localFunc, trueDest, falseDest)
            );
            return syntaxTree->irCode;
        case NOT_LOGIC_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode,
                generateControlFlowIR(syntaxTree->child1, localFunc, falseDest, trueDest)
            );
            return syntaxTree->irCode;
    }
    
    Symbol *src1, *src2;
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child1, localFunc, false));
    syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree->child2, localFunc, false));
    src1 = (syntaxTree->child1 != NULL) ? syntaxTree->child1->destSymbol : NULL;
    src2 = (syntaxTree->child2 != NULL) ? syntaxTree->child2->destSymbol : NULL;
    switch (syntaxTree->op) {
        case EQ_REL_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(IF_EQ_IROP, src1, src2, NULL, trueDest, falseDest)
            );
            break;
        case NEQ_REL_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(IF_NEQ_IROP, src1, src2, NULL, trueDest, falseDest)
            );
            break;
        case LTE_REL_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(IF_LTE_IROP, src1, src2, NULL, trueDest, falseDest)
            );
            break;
        case LT_REL_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(IF_LT_IROP, src1, src2, NULL, trueDest, falseDest)
            );
            break;
        case GTE_REL_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(IF_GTE_IROP, src1, src2, NULL, trueDest, falseDest)
            );
            break;
        case GT_REL_NODEOP:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, 
                IR_Instr_new(IF_GT_IROP, src1, src2, NULL, trueDest, falseDest)
            );
            break;
        default:
            syntaxTree->irCode = IR_Instr_append(syntaxTree->irCode, generateIR(syntaxTree, localFunc, false));
            break;
    }
    return syntaxTree->irCode;
}

void assignSymbolOffsets(Symbol* function) {
    int paramOffset = 4;
    int fpOffset = 0;
    char buffer[128];

    Symbol* actualParameters = function->function_arg;
    Symbol* localSt = function->local_st;

    Symbol* walker = localSt;
    while (walker != NULL) {
        if (SymbolTable_contains(actualParameters, walker->name)) {
            if (walker->type == HEAD_TYPE || walker->type == VOID_TYPE) {
                walker = walker->nextSymbol;
                continue;
            }
            paramOffset += 4;
            walker->frameOffset = paramOffset;
            walker = walker->nextSymbol;
            continue;
        }
        int i;
        switch (walker->type) {
            case INT_TYPE:
            case ADDR_TYPE:
            //fpOffset -= 4 - (4 + fpOffset % 4);
            if ((-fpOffset) % 4 != 0) {
                fpOffset -= (4 - ((-fpOffset) % 4));
            }
            fpOffset -= 4;
            break;
            case CHAR_TYPE:
            fpOffset -= 1;
            break;
            case INTARR_TYPE:
            /* TODO */
            if ((-fpOffset) % 4 != 0) {
                fpOffset -= (4 - ((-fpOffset) % 4));
            }
            for (i = 0; i < walker->array_numElements; i = i + 1) {
                fpOffset -= 4;   
            }
            break;
            case CHARARR_TYPE:
            // fprintf(stdout, "Here I am, assigning offsets for a local chararr variable %s\n", walker->name);
            if (walker->codegen_isPtr) {
                if ((-fpOffset) % 4 != 0) {
                    fpOffset -= (4 - ((-fpOffset) % 4));
                }
                fpOffset -= 4;
            } else {
                fpOffset -= (4 - (walker->array_numElements % 4));
                for (i = 0; i < walker->array_numElements; i = i + 1) {
                    fpOffset -= 1;
                }
            }
            break;
            case HEAD_TYPE:
            break;
            default:
            snprintf(buffer, sizeof(buffer), "Unable to assign symbol table offset to symbol %s of type %s", walker->name, SymbolType_toStr(walker->type));
            yyerror(buffer);
        }
        walker->frameOffset = fpOffset;
        walker = walker->nextSymbol;
    }

    function->function_nargs = (paramOffset-1)/4;
    if ((-fpOffset) % 4 != 0) {
        fpOffset -= (4 - ((-fpOffset) % 4));
    }
    function->function_fpOffset = fpOffset;
}

void printIRToMIPS(IR_Instr* instr, FILE* outputFile) {
    // IR_Instr_print(instr, outputFile);
    int nargs; char buf[128];
    fprintf(outputFile, "#"); IR_Instr_print(instr, outputFile); fprintf(outputFile, "\n");
    switch (instr->operation) {
        case CONST_IROP:
            if (instr->dest->type == INT_TYPE) {
                fprintf(outputFile, "lui $t0, %d\n", instr->val.i >> 16);
                fprintf(outputFile, "ori $t0, %d\n", instr->val.i & 0x0000ffff);
            } else if (instr->dest->type == CHAR_TYPE) {
                fprintf(outputFile, "add $t0, $0, $0\n");
                fprintf(outputFile, "ori $t0, %d\n", instr->val.c & 0x000000ff);
            } else if (instr->dest->type == CHARARR_TYPE) {
                fprintf(outputFile, "la $t0, _%s\n", instr->val.s->identifier);
            // } else if (instr->dest->type == ADDR_TYPE) {
            //     fprintf(outputFile, "la $t0, _%s\n", instr->val.s != NULL ? instr->val.s->identifier : "BLAH");
            } else {
                /* TODO? */
            }
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case ASSG_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case ADDR_OF_IROP:
            loadAddrTo("$t0", instr->src1, outputFile);
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case MEM_DEREF_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            //loadVarTo("$t1", instr->src2, outputFile);
            //fprintf(outputFile, "add $t0, $t0, $t1\n");
            if (instr->src1->type == CHARARR_TYPE) {
                fprintf(outputFile, "lb $t0, 0($t0)\n");
            } else if (instr->src1->type == INTARR_TYPE) {
                fprintf(outputFile, "lw $t0, 0($t0)\n");
            } else {
                yyerror("HELP I TRIED TO DO A MEM_DEREF_IROP AND I DON'T KNOW WHAT TO DO!");
            }
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
            /*
        case ARRAY_LD_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            if (instr->dest->type == INT_TYPE) {
                fprintf(outputFile, "lw $t0, 0($t0)\n");
            } else if (instr->dest->type == CHAR_TYPE) {
                fprintf(outputFile, "lb $t0, 0($t0)\n");
            } else {
                yyerror("Attempted to load array element into a resulting symbol that wasn't of type int or char\n");
            }
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
            */
        case MEM_ASSG_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->dest, outputFile);
            if (instr->dest->type == INTARR_TYPE) {
                fprintf(outputFile, "sw $t0, 0($t1)\n");
            } else if (instr->dest->type == CHARARR_TYPE) {
                fprintf(outputFile, "sb $t0, 0($t1)\n");
            } else {
                yyerror("Attempted to store array element into a destination that wasn't an int array or char array\n");
            }
            break;
            /*
        case ADDR_PLUS_BINARY_IROP:
            if (symbolIsGlobal(instr->src1)) {
                fprintf(outputFile, "la $t0, _%s\n", instr->src1->name);
            } else {
                fprintf(outputFile, "la $t0, %d($fp)\n", instr->src1->frameOffset);
            }
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "add $t0, $t0, $t1\n");
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
            */
        case MINUS_UNARY_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            fprintf(outputFile, "sub $t0, $0, $t0\n");
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case PLUS_BINARY_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "add $t0, $t0, $t1\n");
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case MINUS_BINARY_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "sub $t0, $t0, $t1\n");
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case MULT_BINARY_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            /* RIP upper 32 bits Q-Q */
            fprintf(outputFile, "mul $t0, $t0, $t1\n");
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case DIVIDE_BINARY_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            /* TODO: Is this how we're "supposed" to be doing it? */
            fprintf(outputFile, "div $t0, $t1\n");
            fprintf(outputFile, "mflo $t0\n");
            saveVarFrom("$t0", instr->dest, outputFile);
            break;
        case IF_GT_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "bgt $t0, $t1, _%s\n", instr->trueLab->src1->name);
            if (instr->falseLab != NULL) {
                fprintf(outputFile, "j _%s\n", instr->falseLab->src1->name);
            }
            break;
        case IF_GTE_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "bge $t0, $t1, _%s\n", instr->trueLab->src1->name);
            if (instr->falseLab != NULL) {
                fprintf(outputFile, "j _%s\n", instr->falseLab->src1->name);
            }
            break;
        case IF_EQ_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "beq $t0, $t1, _%s\n", instr->trueLab->src1->name);
            if (instr->falseLab != NULL) {
                fprintf(outputFile, "j _%s\n", instr->falseLab->src1->name);
            }
            break;
        case IF_NEQ_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "bne $t0, $t1, _%s\n", instr->trueLab->src1->name);
            if (instr->falseLab != NULL) {
                fprintf(outputFile, "j _%s\n", instr->falseLab->src1->name);
            }
            break;
        case IF_LTE_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "ble $t0, $t1, _%s\n", instr->trueLab->src1->name);
            if (instr->falseLab != NULL) {
                fprintf(outputFile, "j _%s\n", instr->falseLab->src1->name);
            }
            break;
        case IF_LT_IROP:
            loadVarTo("$t0", instr->src1, outputFile);
            loadVarTo("$t1", instr->src2, outputFile);
            fprintf(outputFile, "blt $t0, $t1, _%s\n", instr->trueLab->src1->name);
            if (instr->falseLab != NULL) {
                fprintf(outputFile, "j _%s\n", instr->falseLab->src1->name);
            }
            break;
        case GOTO_IROP:
            fprintf(outputFile, "j _%s\n", instr->trueLab->src1->name);
            break;
        case PARAM_IROP:
            /* TODO: Ugh */
            if (instr->src1 != NULL && instr->src1->type == CHARARR_TYPE && !symbolIsGlobal(instr->src1) && !instr->src1->codegen_isPtr) {
                fprintf(outputFile, "la $t0, %d($fp)\n", instr->src1->frameOffset);
            } else if (instr->src1 != NULL && instr->src1->type == INTARR_TYPE && !symbolIsGlobal(instr->src1) && !instr->src1->codegen_isPtr) {
                fprintf(outputFile, "la $t0, %d($fp)\n", instr->src1->frameOffset);
            } else if (instr->src1 != NULL && instr->src1->type == CHARARR_TYPE && symbolIsGlobal(instr->src1) && !instr->src1->codegen_isPtr) {
                fprintf(outputFile, "la $t0, _%s\n", instr->src1->name);
            } else if (instr->src1 != NULL && instr->src1->type == INTARR_TYPE && symbolIsGlobal(instr->src1) && !instr->src1->codegen_isPtr) {
                fprintf(outputFile, "la $t0, _%s\n", instr->src1->name);
            } else {
                loadVarTo("$t0", instr->src1, outputFile);
            }
            fprintf(outputFile, "addi $sp, $sp, -4\n");
            fprintf(outputFile, "sw $t0, 0($sp)\n");
            break;
        case CALL_IROP:
            if (instr->src1 != NULL) {
                fprintf(outputFile, "jal _%s\n", instr->src1->name);
                fprintf(outputFile, "addi $sp, $sp, %d\n", 4*instr->src1->function_nargs);
            }
            break;
        case ENTER_IROP:
            fprintf(outputFile, "_%s:\n", instr->src1->name);
            fprintf(outputFile, "addi $sp, $sp, -8\n");
            fprintf(outputFile, "sw $ra, 4($sp)\n");
            fprintf(outputFile, "sw $fp, 0($sp)\n");
            fprintf(outputFile, "add $fp, $0, $sp\n");
            if (instr->src1->function_fpOffset != 0) {
                fprintf(outputFile, "addi $sp, $sp, %d\n", instr->src1->function_fpOffset);
            }
            break;
        case LEAVE_IROP:
            /* Normally where restoring callee-saved registers would go. Ignored for CSC453 */
            break;
        case RETURN_IROP:
            if (instr->src1 != NULL) {
                loadVarTo("$v0", instr->src1, outputFile);
            }
            fprintf(outputFile, "add $sp, $0, $fp\n");
            fprintf(outputFile, "lw $ra, 4($sp)\n");
            fprintf(outputFile, "lw $fp, 0($sp)\n");
            fprintf(outputFile, "addi $sp, $sp, 8\n");
            fprintf(outputFile, "jr $ra\n");
            break;
        case RETRIEVE_IROP:
            saveVarFrom("$v0", instr->dest, outputFile);
            break;
        case LABEL_IROP:
            fprintf(outputFile, "_%s:\n", instr->src1->name);
            break;
    }
    fprintf(outputFile, "\n");
}

void loadVarTo(const char* dest, Symbol* sym, FILE* outputFile) {
    if (dest == NULL || sym == NULL || outputFile == NULL) {
        return;
    }
    if (symbolIsGlobal(sym)) {
        if (sym->type == INT_TYPE || sym->type == INTARR_TYPE) {
            fprintf(outputFile, "lw %s, _%s\n", dest, sym->name);
        } else if (sym->type == CHAR_TYPE) {
            fprintf(outputFile, "lb %s, _%s\n", dest, sym->name);
        } else if (sym->type == CHARARR_TYPE) {
            /* TODO? */
            fprintf(outputFile, "la %s, _%s\n", dest, sym->name);
        } else {
            /* TODO */
            char buf[128];
            snprintf(buf, sizeof(buf), "Tried to loadVarTo with an unsupported symbol type of %s\n", SymbolType_toStr(sym->type));
            yyerror(buf);
        }
    } else {
        if (sym->type == INT_TYPE || sym->type == INTARR_TYPE || sym->type == ADDR_TYPE) {
            fprintf(outputFile, "lw %s, %d($fp)\n", dest, sym->frameOffset);
        } else if (sym->type == CHAR_TYPE) {
            fprintf(outputFile, "lb %s, %d($fp)\n", dest, sym->frameOffset);
        } else if (sym->type == CHARARR_TYPE) {
            /* TODO? */
            fprintf(outputFile, "lw %s, %d($fp)\n", dest, sym->frameOffset);
        } else {
            /* TODO */
            char buf[128];
            snprintf(buf, sizeof(buf), "Tried to loadVarTo with an unsupported symbol type of %s\n", SymbolType_toStr(sym->type));
            yyerror(buf);
        }
    }
}

void loadAddrTo(const char* dest, Symbol* sym, FILE* outputFile) {
    if (dest == NULL || sym == NULL || outputFile == NULL) {
        return;
    }
    if (symbolIsGlobal(sym)) {
        fprintf(outputFile, "la %s, _%s\n", dest, sym->name);
    } else if (symbolIsParam(sym)) {
        if (sym->type == CHARARR_TYPE || sym->type == INTARR_TYPE) {
            fprintf(outputFile, "lw %s, %d($fp)\n", dest, sym->frameOffset);
        } else {
            fprintf(outputFile, "la %s, %d($fp)\n", dest, sym->frameOffset);
        }
    } else {
        fprintf(outputFile, "la %s, %d($fp)\n", dest, sym->frameOffset);
    }
}

void saveVarFrom(const char* src, Symbol* sym, FILE* outputFile) {
    if (src == NULL || sym == NULL || outputFile == NULL) {
        return;
    }
    // fprintf(stdout, "Here in saveVarFrom, symbol %s has a frameoffset of %d\n", sym->name, sym->frameOffset);
    if (symbolIsGlobal(sym)) {
        if (sym->type == INT_TYPE || sym->type == INTARR_TYPE) {
            fprintf(outputFile, "sw %s, _%s\n", src, sym->name);
        } else if (sym->type == CHAR_TYPE) {
            fprintf(outputFile, "sb %s, _%s\n", src, sym->name);
        } else if (sym->type == CHARARR_TYPE) {
            /* TODO? */
            fprintf(outputFile, "sw %s, _%s\n", src, sym->name);
        } else if (sym->type == ADDR_TYPE) {
            fprintf(outputFile, "sw %s, _%s\n", src, sym->name);
        } else {
            /* TODO */
        }
    } else {
        if (sym->type == INT_TYPE || sym->type == INTARR_TYPE) {
            fprintf(outputFile, "sw %s, %d($fp)\n", src, sym->frameOffset);
        } else if (sym->type == CHAR_TYPE) {
            fprintf(outputFile, "sb %s, %d($fp)\n", src, sym->frameOffset);
        } else if (sym->type == CHARARR_TYPE) {
            /* TODO? */
            fprintf(outputFile, "sw %s, %d($fp)\n", src, sym->frameOffset);
        } else if (sym->type == ADDR_TYPE) {
            // if (sym->codegen_isPtr) {
            //     fprintf(outputFile, "lw $t7, %d($fp)\n", sym->frameOffset);
            //     fprintf(outputFile, "sw %s, 0($t7)\n", src);
            // } else {
                fprintf(outputFile, "sw %s, %d($fp)\n", src, sym->frameOffset);    
            // }
        } else {
            /* TODO */
        }
    }
}

bool symbolIsGlobal(Symbol* sym) {
    return sym->frameOffset == 0;
}

bool symbolIsParam(Symbol* sym) {
    return sym->frameOffset > 0;
}

Symbol* getTemp(SymbolType type, Symbol* symbolTable) {
    char errBuff[128];
    if (type != INT_TYPE && type != CHAR_TYPE && type != INTARR_TYPE && type != CHARARR_TYPE && type != ADDR_TYPE) {
        snprintf(errBuff, sizeof(errBuff), "A temporary variable of type %s was requested and that don't make no darn sense", SymbolType_toStr(type));
        yyerror(errBuff);
        return NULL;
    }
    char nameBuffer[32];
    snprintf(nameBuffer, sizeof(nameBuffer), "_TEMP_VAR_%d", tmpCounter++);
    Symbol* newSym = Symbol_new(nameBuffer, type);
    SymbolTable_append(symbolTable, newSym);
    return newSym;
}

IR_Instr* getLabel() {
    char nameBuffer[32];
    snprintf(nameBuffer, sizeof(nameBuffer), "_LABEL_%d", labelCounter++);
    return IR_Instr_new(LABEL_IROP, Symbol_new(nameBuffer, UNASSIGNED_TYPE), NULL, NULL, NULL, NULL);
}

IR_Instr* useRecursionToBuildTheParamInstructionsInReverse(IR_Instr* head, Node* arg) {
    if (arg == NULL) {
        return NULL;
    }
    head = useRecursionToBuildTheParamInstructionsInReverse(head, arg->nextStmt);
    return IR_Instr_append(head, IR_Instr_new(PARAM_IROP, arg->destSymbol, NULL, NULL, NULL, NULL));
}

IR_Instr* IR_Instr_new(IR_Op op, Symbol* src1, Symbol* src2, Symbol* dest, IR_Instr* trueLab, IR_Instr* falseLab) {
    IR_Instr* instr;
    if (NULL != (instr = (IR_Instr*)calloc(sizeof(IR_Instr), 1))) {
        instr->operation = op;
        instr->src1 = src1;
        instr->src2 = src2;
        instr->dest = dest;
        instr->trueLab = trueLab;
        instr->falseLab = falseLab;
    } else {
        fprintf(stderr, "Fatal Error: Unable to allocate memory for new IR instruction!\n");
        exit(1);
    }
    return instr;
}

void IR_Instr_free(IR_Instr* instr) {
    free(instr);
}

IR_Instr* IR_Instr_append(IR_Instr* instr1, IR_Instr* instr2) {
    if (instr1 == NULL) {
        return instr2;
    }
    if (instr2 == NULL) {
        return instr1;
    }
    IR_Instr* walker = instr1;
    while (walker->nextInstr != NULL) {
        walker = walker->nextInstr;
    }
    walker->nextInstr = instr2;
    return instr1;
}

void IR_InstrList_print(IR_Instr* head, FILE* file) {
    while (head != NULL) {
        IR_Instr_print(head, file);
        fprintf(file, "\n");
        head = head->nextInstr;
    }
}

void IR_Instr_print(IR_Instr* instr, FILE* file) {
    fprintf(file, "Op: %s", IR_Op_toStr(instr->operation));
    if (instr->src1 != NULL) { fprintf(file, ", Src1: %s", instr->src1->name); }
    if (instr->src2 != NULL) { fprintf(file, ", Src2: %s", instr->src2->name); }
    if (instr->dest != NULL) { fprintf(file, ", Dest: %s (type: %s)", instr->dest->name, SymbolType_toStr(instr->dest->type)); }
    if (instr->trueLab != NULL) { fprintf(file, ", TrueLabel: %s", instr->trueLab->src1->name); }
    if (instr->falseLab != NULL) { fprintf(file, ", FalseLabel: %s", instr->falseLab->src1->name); }
    
    // if (instr->val.s != NULL) { fprintf(file, ", string table identifier: %s", instr->val.s->identifier); } 
    // else { fprintf(file, ", intVal: %d, charVal: %c", instr->val.i, instr->val.c); }
}

char* IR_Op_toStr(IR_Op op) {
    switch (op) {
        case CONST_IROP:
            return "Constant assignment IR Op";
            break;
        case ASSG_IROP:
            return "Assignment IR Op";
            break;
        case ADDR_OF_IROP:
            return "Address of IR Op";
            break;
        case MEM_DEREF_IROP:
            return "Memory dereference IR Op";
            break;
        case MEM_ASSG_IROP:
            return "Memory assignment IR Op";
            break;
            /*
        case ARRAY_LD_IROP:
            return "Array Load IR Op";
            break;
        case ARRAY_ST_IROP:
            return "Array Store IR Op";
            break;
            */
        case MINUS_UNARY_IROP:
            return "Unary Minus IR Op";
            break;
            /*
        case ADDR_PLUS_BINARY_IROP:
            return "Address Plus IR Op";
            break;
            */
        case PLUS_BINARY_IROP:
            return "Plus IR Op";
            break;
        case MINUS_BINARY_IROP:
            return "Binary Minus IR Op";
            break;
        case MULT_BINARY_IROP:
            return "Mult IR Op";
            break;
        case DIVIDE_BINARY_IROP:
            return "Divide IR Op";
            break;
        case IF_GT_IROP:
            return "If GT IR Op";
            break;
        case IF_GTE_IROP:
            return "If GTE IR Op";
            break;
        case IF_EQ_IROP:
            return "If EQ IR Op";
            break;
        case IF_LTE_IROP:
            return "If LTE IR Op";
            break;
        case IF_LT_IROP:
            return "If LT IR Op";
            break;
        case GOTO_IROP:
            return "Goto IR Op";
            break;
        case PARAM_IROP:
            return "Param IR Op";
            break;
        case CALL_IROP:
            return "Call IR Op";
            break;
        case ENTER_IROP:
            return "Enter IR Op";
            break;
        case LEAVE_IROP:
            return "Leave IR Op";
            break;
        case RETURN_IROP:
            return "Return IR Op";
            break;
        case RETRIEVE_IROP:
            return "Retrieve IR Op";
            break;
        case LABEL_IROP:
            return "Label IR Op";
            break;
        default:
            return "Something that needs a toString case statement";
            break;
    }
}