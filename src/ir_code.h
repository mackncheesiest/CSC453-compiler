#ifndef IR_INSTR_H
#define IR_INSTR_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "parsetree.h"

/* Forward declaration elminates problems with parsetree <-> ir_code import cycle */
typedef struct Node_T Node;
typedef struct StringTable_T StringTable;

typedef enum {
    CONST_IROP,
    ASSG_IROP,

    ADDR_OF_IROP,
    MEM_DEREF_IROP,
    MEM_ASSG_IROP,

    MINUS_UNARY_IROP,

    PLUS_BINARY_IROP,
    MINUS_BINARY_IROP,
    MULT_BINARY_IROP,
    DIVIDE_BINARY_IROP,  

    IF_GT_IROP,
    IF_GTE_IROP,
    IF_EQ_IROP,
    IF_NEQ_IROP,
    IF_LTE_IROP,
    IF_LT_IROP,
    GOTO_IROP,

    PARAM_IROP,
    CALL_IROP,
    ENTER_IROP,
    LEAVE_IROP,
    RETURN_IROP,
    RETRIEVE_IROP,

    LABEL_IROP
} IR_Op;

typedef struct IR_LitVal_T {
    int i;
    char c;
    StringTable* s;
} IR_LitVal;

typedef struct IR_Instr_T {
    IR_Op operation;

    Symbol* src1;
    Symbol* src2;
    Symbol* dest;
    
    struct IR_Instr_T* trueLab;
    struct IR_Instr_T* falseLab;

    IR_LitVal val;

    //bool isLVal;

    struct IR_Instr_T* nextInstr;
} IR_Instr;

extern void initializeOutput(FILE* outputFile);
extern void generateFunction(Node* syntaxTree, Symbol* global_st, Symbol* localFuncSym, FILE* outputFile);
extern void generateGlobalCode(Symbol* global_st, StringTable* strTab, FILE* outputFile);

extern IR_Instr* generateIR(Node* syntaxTree, Symbol* localFunc, bool isLVal);
extern IR_Instr* generateControlFlowIR(Node* syntaxTree, Symbol* localFunc, IR_Instr* trueDest, IR_Instr* falseDest);
extern void assignSymbolOffsets(Symbol* function);
extern void printIRToMIPS(IR_Instr* instrList, FILE* outputFile);

extern void loadVarTo(const char* dest, Symbol* sym, FILE* outputFile);
extern void loadAddrTo(const char* dest, Symbol* sym, FILE* outputFile);
extern void saveVarFrom(const char* src, Symbol* sym, FILE* outputFile);
extern bool symbolIsParam(Symbol* sym);
extern bool symbolIsGlobal(Symbol* sym);
extern Symbol* getTemp(SymbolType type, Symbol* symbolTable);
extern IR_Instr* getLabel();
extern IR_Instr* useRecursionToBuildTheParamInstructionsInReverse(IR_Instr* head, Node* stmt);

extern IR_Instr* IR_Instr_new(IR_Op op, Symbol* src1, Symbol* src2, Symbol* dest, IR_Instr* trueLab, IR_Instr* falseLab);
extern void IR_Instr_free(IR_Instr* instr);
extern IR_Instr* IR_Instr_append(IR_Instr* instr1, IR_Instr* instr2);
extern void IR_InstrList_print(IR_Instr* head, FILE* file);
extern void IR_Instr_print(IR_Instr* instr, FILE* file);
extern char* IR_Op_toStr(IR_Op op);

#endif