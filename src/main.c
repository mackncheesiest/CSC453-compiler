#include <stdlib.h>
#include <string.h>
#include <argp.h>
#include "symboltable.h"
#include "parsetree.h"

extern int yyparse();
extern FILE* yyin;
extern Symbol* st;
extern Node* treeRoot;
extern FILE* outputFile;

FILE* binaryOutputFile;
bool generateBinary;

const char *program_version = "<C-- Compiler 0.1>";
const char *bug_address = "<mackncheesiest@gmail.com>";

static char doc[] = "A simple compiler for a subset of C using Flex/Bison. Either pass in a file as a commandline argument or through stdin.";
static char args_doc[] = "[inputFile]";

static struct argp_option options[] = {
    {"output", 'o', "FILE", 0, "Output to FILE instead of standard output"},
    {"generate-binary", 'b', 0, 0, "Save assembled MIPS binary to the output as opposed to MIPS assembly"},
    { 0 }
};

struct arguments {
    char *inputFile;
    char *outputFile;
    bool generateBinary;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;

  switch (key) {
    case 'o':
      arguments->outputFile = arg;
      break;
    case 'b':
      arguments->generateBinary = true;
    case ARGP_KEY_ARG:
      /* We don't accept any arguments that don't require flags */
      if (state->arg_num >= 1) {
          argp_usage (state);
      }
      else if (state->arg_num == 0) {
          arguments->inputFile = arg;
      }
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

int main(int argc, char** argv) {
    struct arguments arguments;
    arguments.inputFile = "-";
    arguments.outputFile = "-";
    arguments.generateBinary = false;
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (strcmp(arguments.inputFile, "-") != 0) {
        FILE* fd = fopen(arguments.inputFile, "r");
        if (fd == NULL) {
            fprintf(stderr, "Fatal Error: Unable to open file [%s] for input\nAborting compilation...\n", arguments.inputFile);
            exit(1);
        }
        yyin = fd;
    } else {
        yyin = stdin;
    }
    if (strcmp(arguments.outputFile, "-") != 0) {
        if (arguments.generateBinary) {
            outputFile = tmpfile();
        } else {
            FILE* fd = fopen(arguments.outputFile, "w");
            if (fd == NULL) {
                fprintf(stderr, "Fatal Error: Unable to open file [%s] for output\nAborting compilation...\n", arguments.outputFile);
                exit(1);
            }
            outputFile = fd;
        }
    } else {
        outputFile = stdout;
    }
    generateBinary = arguments.generateBinary;
    int exitStatus = yyparse();
    if (generateBinary) {
        rewind(outputFile);
        FILE* realOutput = fopen(arguments.outputFile, "w");
        if (realOutput == NULL) {
            fprintf(stderr, "Fatal Error: Unable to open file [%s] for output\nAborting compilation...\n", arguments.outputFile);
            exit(1);
        }
        char ch;
        /* TODO: Actually generate MIPS binary here */
        while ((ch = fgetc(outputFile)) != EOF) {
            fputc(ch, realOutput);
        }
        fclose(outputFile);
        fclose(realOutput);
    }
    SymbolTable_free(st, true);
    return exitStatus;
}
