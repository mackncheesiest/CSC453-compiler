#include <stdlib.h>
#include <stdio.h>
#include "parsetree.h"
#include "symboltable.h"

int strConstNum = 0;

char* NodeOperator_toStr(Operator type) {
    switch(type) {
    case UNASSIGNED_NODEOP:
        return "Unassigned node op";
    case NOOP_NODEOP:
        return "NOOP node op";
    case CONST_NODEOP:
        return "Constant node op";
    case VAR_NODEOP:
        return "Variable node op";
    case FUNC_CALL_NODEOP:
        return "Func call node op";
    case MINUS_UNARY_NODEOP:
        return "Unary minus node op";
    case NOT_LOGIC_NODEOP:
        return "Unary negation node op";
    case ASSG_NODEOP:
        return "Assignment node op";
    case RETURN_NODEOP:
        return "Return node op";
    case PLUS_NODEOP:
        return "Plus node op";
    case MINUS_BINARY_NODEOP:
        return "Binary minus node op";
    case MULT_NODEOP:
        return "Mult node op";
    case DIVIDE_NODEOP:
        return "Division node op";
    case EQ_REL_NODEOP:
        return "EQ relational node op";
    case NEQ_REL_NODEOP:
        return "NEQ relational node op";
    case LTE_REL_NODEOP:
        return "LTE relational node op";
    case LT_REL_NODEOP:
        return "LT relational node op";
    case GTE_REL_NODEOP:
        return "GTE relational node op";
    case GT_REL_NODEOP:
        return "GT relational node op";
    case AND_LOGIC_NODEOP:
        return "AND logical node op";
    case OR_LOGIC_NODEOP:
        return "OR logical node op";
    case ARRAY_INDEX_NODEOP:
        return "Array indexing node op";
    case IF_STMT_NODEOP:
        return "If statement node op";
    case FOR_STMT_NODEOP:
        return "For statement node op";
    case WHILE_STMT_NODEOP:
        return "While statment node op";
    default:
        return "Something that needs a toString case statement";
    }
}

Node* Node_new() {
    Node* node;
    if (NULL != (node = (Node*)calloc(sizeof(Node), 1))) {
        return node;
    } else {
        fprintf(stderr, "Fatal Error: Unable to allocate memory for a new Symbol!\n");
        /* TODO: Consider using backtrace/execinfo.h to dump a stacktrace on fatal errors like this */
        exit(1);
    }
}

void Node_free(Node* node) {
    if (node == NULL) {
        return;
    }
    free(node);
}

StringTable* StringTable_new(char* stringLiteral) {
    StringTable* str;
    char idBuf[32];
    if (NULL != (str = (StringTable*)calloc(sizeof(StringTable), 1))) {
        if (NULL != ((str->strVal) = (char*)calloc(strlen(stringLiteral)+1, 1))) {
            strcpy(str->strVal, stringLiteral);
        }
        else {
            fprintf(stderr, "Fatal Error: Unable to allocate memory for new StringTable entry's string value!\n");
            exit(1);
        }
        snprintf(idBuf, sizeof(idBuf), "_STR%d", strConstNum++);
        if (NULL != (str->identifier = (char*)calloc(strlen(idBuf)+1, 1))) {
            strcpy(str->identifier, idBuf);
        } else {
            fprintf(stderr, "Fatal Error: Unable to allocate memory for new StringTable entry's identifier value!\n");
            exit(1);
        }
    } else {
        fprintf(stderr, "Fatal Error: Unable to allocate memory for new StringTable entry!\n");
        exit(1);
    }
    return str;
}

StringTable* StringTable_append(StringTable* head, StringTable* entry) {
    if (head == NULL) {
        return entry;
    }
    if (entry == NULL) {
        return head;
    }
    StringTable* walker = head;
    while (walker->nextEntry != NULL) {
        walker = walker->nextEntry;
    }
    walker->nextEntry = entry;
    return head;
}

void StringTable_free(StringTable* entry) {
    free(entry->identifier);
    free(entry->strVal);
    free(entry);
}

void Tree_free(Node* root) {
    if (root == NULL) {
        return;
    }
    Tree_free(root->child1); 
    Tree_free(root->child2);
    Tree_free(root->child3); 
    Tree_free(root->child4);
    Symbol_free(root->symbol);
    free(root);
}

void Tree_print(Node* root, int depth) {
    int i;
    if (root == NULL) {
        return;
    }
    for (i = 0; i < depth; i++) {
        fprintf(stdout, "\t");
    }
    fprintf(stdout, "Operator: [%s], resultType: [%s], intVal: [%d], charVal: [%c], boolVal: [%s]", NodeOperator_toStr(root->op), SymbolType_toStr(root->resultType), root->intVal, root->charVal, root->boolVal ? "true" : "false");
    if (root->stringVal != NULL) {
        fprintf(stdout, ", stringVal identifier: [%s], stringVal value: [%s]", root->stringVal->identifier, root->stringVal->strVal);
    }
    fprintf(stdout, "\n");
    Symbol_print(root->symbol, depth+1);
    Symbol_print(root->destSymbol, depth+1);
    //IR_InstrList_print(root->irCode, stdout);
    
    // for (i = 0; i < depth; i++) {
    //     fprintf(stdout, "\t");
    // }
    // fprintf(stdout, "Child 1:\n");
    
    Tree_print(root->child1, depth+1);
    
    // for (i = 0; i < depth; i++) {
    //     fprintf(stdout, "\t");
    // }
    // fprintf(stdout, "Child 2:\n");
    
    Tree_print(root->child2, depth+1);
    
    // for (i = 0; i < depth; i++) {
    //     fprintf(stdout, "\t");
    // }
    // fprintf(stdout, "Child 3:\n");
    
    Tree_print(root->child3, depth+1);
    
    // for (i = 0; i < depth; i++) {
    //     fprintf(stdout, "\t");
    // }
    // fprintf(stdout, "Child 4:\n");
    
    Tree_print(root->child4, depth+1);

    // for (i = 0; i < depth; i++) {
    //     fprintf(stdout, "\t");
    // }
    // fprintf(stdout, "Next statement:\n");
    
    Tree_print(root->nextStmt, depth);
}