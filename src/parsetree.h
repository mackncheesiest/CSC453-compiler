#ifndef PARSETREE_H
#define PARSETREE_H
#include "symboltable.h"
#include "ir_code.h"

typedef struct IR_Instr_T IR_Instr;

typedef enum Operator_T {
    UNASSIGNED_NODEOP,
    
    NOOP_NODEOP,

    CONST_NODEOP,
    VAR_NODEOP,
    FUNC_CALL_NODEOP,

    MINUS_UNARY_NODEOP,
    
    ASSG_NODEOP,
    RETURN_NODEOP,

    PLUS_NODEOP,
    MINUS_BINARY_NODEOP,
    MULT_NODEOP,
    DIVIDE_NODEOP,

    EQ_REL_NODEOP,
    NEQ_REL_NODEOP,
    LTE_REL_NODEOP,
    LT_REL_NODEOP,
    GTE_REL_NODEOP,
    GT_REL_NODEOP,
    
    AND_LOGIC_NODEOP,
    OR_LOGIC_NODEOP,
    NOT_LOGIC_NODEOP,

    ARRAY_INDEX_NODEOP,

    IF_STMT_NODEOP,
    FOR_STMT_NODEOP,
    WHILE_STMT_NODEOP
} Operator;

extern char* NodeOperator_toStr(Operator op);

typedef struct StringTable_T {
    char* identifier;
    char* strVal;
    struct StringTable_T* nextEntry;
} StringTable;

typedef struct Node_T {
    Operator op;

    SymbolType resultType;

    int intVal;
    int* intArrVal;
    char charVal;
    StringTable* stringVal;
    bool boolVal;
    Symbol* symbol;

    struct Node_T* child1;
    struct Node_T* child2;
    struct Node_T* child3;
    struct Node_T* child4;

    struct Node_T* nextStmt;

    Symbol* destSymbol;
    IR_Instr* irCode;
} Node;

extern Node* Node_new();
extern void Node_free(Node* node);

extern StringTable* StringTable_new(char* stringLiteral);
extern StringTable* StringTable_append(StringTable* head, StringTable* entry);
extern void StringTable_free(StringTable* entry);

extern void Tree_free(Node* root);
extern void Tree_print(Node* root, int depth);

#endif