#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "symboltable.h"

char* SymbolType_toStr(SymbolType type) {
    switch(type) {
    case UNASSIGNED_TYPE:
        return "Unassigned Type";
    case INT_TYPE:
        return "int";
    case CHAR_TYPE:
        return "char";
    case INTARR_TYPE:
        return "int[]";
    case CHARARR_TYPE:
        return "char[]";
    case VOID_TYPE:
        return "void";
    case NONARR_TYPE:
        return "non array type";
    case ARR_TYPE:
        return "array type";
    case BOOL_TYPE:
        return "bool";
    case FUNC_NODEF_TYPE:
        return "function prototype";
    case EXTERN_FUNC_NODEF_TYPE:
        return "extern function prototype";
    case FUNC_HASDEF_TYPE:
        return "function definition";
    case ERROR_TYPE:
        return "error";
    case HEAD_TYPE:
        return "head symbol";
    case ADDR_TYPE:
        return "address";
    default:
        return "Something that needs a toString case statement";
    }
}

Symbol* Symbol_new(char* name, SymbolType type) {
    Symbol* s;
    if (NULL != (s = (Symbol*)calloc(sizeof(Symbol), 1))) {
        if (NULL != ((s->name) = (char*)calloc(strlen(name)+1, 1))) {
            strcpy(s->name, name);
            s->type = type;
            return s;
        }
        else {
            fprintf(stderr, "Fatal Error: Unable to allocate memory for new Symbol's identifier!\n");
            exit(1);
        }
    } else {
        fprintf(stderr, "Fatal Error: Unable to allocate memory for a new Symbol!\n");
        /* TODO: Consider using backtrace/execinfo.h to dump a stacktrace on fatal errors like this */
        exit(1);
    }
}

Symbol* Symbol_copy(Symbol* source) {
    if (source == NULL) {
        return NULL;
    }
    Symbol* symCopy = Symbol_new(source->name, source->type);
    symCopy->array_numElements = source->array_numElements;
    symCopy->function_returnType = source->function_returnType;
    /* TODO: When copying a Function symbol, some extra work is needed to ensure that all necessary function_arg/local_st 
       symbols come with without being coupled to the source symbol through old pointers */
    return symCopy;
}

Symbol* Symbol_setType(Symbol* symbol, SymbolType type) {
    symbol->type = type;
    return symbol;
}

Symbol* Symbol_setNumArrElements(Symbol* symbol, int numElements) {
    symbol->array_numElements = numElements;
    return symbol;
}

Symbol* Symbol_setArgSymbol(Symbol* func, Symbol* argument) {
    func->function_arg = argument;
    return func;
}

Symbol* Symbol_setReturnType(Symbol* func, SymbolType retType) {
    func->function_returnType = retType;
    return func;
}

void Symbol_free(Symbol* symbol) {
    if (symbol == NULL) {
        return;
    }
    if (symbol->name != NULL) {
        free(symbol->name);
    }
    Symbol_free(symbol->function_arg);
    free(symbol);
}

void Symbol_print(Symbol* symbol, int depth) {
    int i;
    if (symbol == NULL || (symbol->type == HEAD_TYPE && symbol->nextSymbol == NULL)) {
        return;
    }
    symbol = ((symbol->type == HEAD_TYPE) ? symbol->nextSymbol : symbol);
    for (i = 0; i < depth; i++) {
        fprintf(stdout, "\t");
    }
    Symbol* walker;
    switch(symbol->type) {
        case INT_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type int, Frame offset %d\n", symbol, symbol->name, symbol->frameOffset);
        break;
        case CHAR_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type char, Frame offset %d\n", symbol, symbol->name, symbol->frameOffset);
        break;
        case INTARR_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type int[], Size %d, Frame offset %d\n", symbol, symbol->name, symbol->array_numElements, symbol->frameOffset);
        break;
        case CHARARR_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type char[], Size %d, Frame offset %d\n", symbol, symbol->name, symbol->array_numElements, symbol->frameOffset);
        break;
        case FUNC_NODEF_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type function, Arg types [", symbol, symbol->name);
        walker = symbol->function_arg;
        while(walker != NULL) {
            fprintf(stdout, "%s", SymbolType_toStr(walker->type));
            walker = walker->nextSymbol;
            if (walker != NULL) {
                fprintf(stdout, ", ");
            }
        }
        fprintf(stdout, "], Ret type %s, Num Args %d, Frame Size %d\n", SymbolType_toStr(symbol->function_returnType), symbol->function_nargs, symbol->function_fpOffset);
        break;
        case EXTERN_FUNC_NODEF_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type extern function, Arg types [", symbol, symbol->name);
        walker = symbol->function_arg;
        while(walker != NULL) {
            fprintf(stdout, "%s", SymbolType_toStr(walker->type));
            walker = walker->nextSymbol;
            if (walker != NULL) {
                fprintf(stdout, ", ");
            }
        }
        fprintf(stdout, "], Ret type %s, Num Args %d, Frame Size %d\n", SymbolType_toStr(symbol->function_returnType), symbol->function_nargs, symbol->function_fpOffset);
        break;
        case FUNC_HASDEF_TYPE:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type function with def, Arg types [", symbol, symbol->name);
        walker = symbol->function_arg;
        while(walker != NULL) {
            fprintf(stdout, "%s", SymbolType_toStr(walker->type));
            walker = walker->nextSymbol;
            if (walker != NULL) {
                fprintf(stdout, ", ");
            }
        }
        fprintf(stdout, "], Ret type %s, Num Args %d, Frame Size %d\n", SymbolType_toStr(symbol->function_returnType), symbol->function_nargs, symbol->function_fpOffset);
        break;
        default:
        fprintf(stdout, "Symbol: Addr %p, Name %s, Type %s, Frame offset %d\n", symbol, symbol->name, SymbolType_toStr(symbol->type), symbol->frameOffset);
        break;
    }
}

Symbol* SymbolTable_new() {
    Symbol* sym = Symbol_new("**ILLEGAL-ID**", HEAD_TYPE);
    return sym;
}

Symbol* SymbolTable_append(Symbol* head, Symbol* symbol) {
    if (head == NULL || symbol == NULL) {
        return head;
    }

    Symbol* walker = head;
    while(walker->nextSymbol != NULL) {
        walker = walker->nextSymbol;
    }
    walker->nextSymbol = symbol;

    return head;
}

Symbol* SymbolTable_remove(Symbol* head, char* name) {
    // fprintf(stderr, "[symboltable] Removing symbol %s\n", name);
    if (head == NULL || name == NULL) {
        return NULL;
    }

    Symbol* removalSym = head;
    Symbol* prev = removalSym;
    while(strcmp(removalSym->name, name) && removalSym->nextSymbol) {
        prev = removalSym;
        removalSym = removalSym->nextSymbol;
    }
    if (removalSym == NULL) {
        return head;
    }
    /* If removalSym == prev, then both == head, and we're removing the head node (or this one entry loops. In that case, y tho?) */
    else if (strcmp(removalSym->name, name) == 0 && prev == removalSym) {
        prev = prev->nextSymbol; /* Same as prev->nextSymbol. They never advanced since we're removing head */
        return prev;
    }
    else if (strcmp(removalSym->name, name) == 0 && prev != removalSym) {
        prev->nextSymbol = removalSym->nextSymbol;
        return head;
    } else {
        return head;
    }
}

Symbol* SymbolTable_get(Symbol* head, char* name) {
    if (head == NULL || name == NULL) {
        return NULL;
    }

    Symbol* walker = head;
    while(strcmp(walker->name, name) && walker->nextSymbol) {
        walker = walker->nextSymbol;
    }
    if (strcmp(walker->name, name) == 0) {
        return walker;
    }
    return NULL;
}

bool SymbolTable_contains(Symbol* head, char* name) {
    if (SymbolTable_get(head, name) != NULL) {
        return true;
    }
    return false;
}

int SymbolTable_length(Symbol* head) {
    if (head == NULL) {
        return 0;
    }

    Symbol* walker = head;
    int length=1;
    while(walker->nextSymbol != NULL) {
        walker = walker->nextSymbol;
        length++;
    }
    fprintf(stderr, "\n");
    return length;
}

void SymbolTable_free(Symbol* head, bool freeChildren) {
    if (head == NULL) {
        return;
    }
    if (freeChildren) {
        Symbol* childSymbol = head->nextSymbol;
        while(childSymbol != NULL) {
            Symbol_free(head);
            head = childSymbol;
            childSymbol = childSymbol->nextSymbol;
        }
    }
    Symbol_free(head);
}

void SymbolTable_print(Symbol* head, int depth) {
    int i;
    if (head == NULL || (head->type == HEAD_TYPE && head->nextSymbol == NULL)) {
        return;
    }
    Symbol* walker = ((head->type == HEAD_TYPE) ? head->nextSymbol : head);
    while (walker) {
        Symbol_print(walker, depth);
        walker = walker->nextSymbol;
    }
}
