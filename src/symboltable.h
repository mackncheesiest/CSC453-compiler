/* Inspired by the Ch. 7 List interface + Implementation from David Hanson's C Interfaces and Implementations */
#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <stdbool.h>
#include <string.h>

typedef enum SymbolType_T {
    /* taking advantage of the fact that I usually calloc everything, all calloced Symbols should have types of UNASSIGNED_TYPE */
    UNASSIGNED_TYPE,
    /* explicit types */
    INT_TYPE,
    CHAR_TYPE,
    INTARR_TYPE,
    CHARARR_TYPE,
    VOID_TYPE,
    /* types used internally by parser */
    NONARR_TYPE,
    ARR_TYPE,
    ADDR_TYPE,
    BOOL_TYPE,
    FUNC_NODEF_TYPE,
    EXTERN_FUNC_NODEF_TYPE,
    FUNC_HASDEF_TYPE,
    ERROR_TYPE,
    HEAD_TYPE
} SymbolType;

extern char* SymbolType_toStr(SymbolType type);

typedef struct Symbol_T {
    char* name;
    
    SymbolType type;
    
    int array_numElements;
    
    struct Symbol_T* function_arg;
    struct Symbol_T* local_st;
    SymbolType function_returnType;
    
    struct Symbol_T* nextSymbol;
    
    int frameOffset;
    int function_nargs;
    int function_fpOffset;
    bool codegen_isPtr;
} Symbol;

/*
    Follows a Map-esque interface
    Inserting a value requres the full Symbol (i.e. a key+value), 
    but accessing a symbol only requires its identifier (i.e. a key)

    Split into three sections:
    - The first group deals with modifications to individual Symbol entries themselves
    - The second group deals with modifications to the broader linked list/SymbolTable that a group of Symbols represents
    - The third group deals with the overall stack of symbol tables required for proper parsing operation as you enter/exit scopes
*/

extern Symbol* Symbol_new(char* name, SymbolType type);
extern Symbol* Symbol_copy(Symbol* source);
extern Symbol* Symbol_setType(Symbol* symbol, SymbolType type);
extern Symbol* Symbol_setNumArrElements(Symbol* array, int numElements);
extern Symbol* Symbol_setArgSymbol(Symbol* func, Symbol* argument);
extern Symbol* Symbol_setReturnType(Symbol* func, SymbolType retType);
extern void Symbol_free(Symbol* symbol);
extern void Symbol_print(Symbol* symbol, int depth);

extern Symbol* SymbolTable_new();
extern Symbol* SymbolTable_append(Symbol* head, Symbol* symbol);
extern Symbol* SymbolTable_remove(Symbol* head, char* name);
extern Symbol* SymbolTable_get(Symbol* head, char* name);
extern bool SymbolTable_contains(Symbol* head, char* name);
extern int SymbolTable_length(Symbol* head);
extern void SymbolTable_free(Symbol* head, bool freeChildren);
extern void SymbolTable_print(Symbol* head, int depth);
#endif