#include "catch.hpp"
#include <vector>
#include "../src/target/c--parserspec.tab.h"

extern int yylex();
extern FILE* yyin;

SCENARIO("We can read an empty program", "[lexer]") {
    GIVEN("An empty input file") {
        yyin = fopen("testfiles/lexer/emptyfile.c", "r");

        WHEN("yylex is called") {
            int output = yylex();
            THEN("The output should immediately be zero/EOF") {
                REQUIRE(output == 0);
            }
        }
    }
}

SCENARIO("We can match each of the flex rules we've defined", "[lexer]") {
    GIVEN("A file containing one of each variant of terminal") {
        yyin = fopen("testfiles/lexer/allterminals.c", "r");

        WHEN("yylex is repeatedly called") {
            std::vector<int> tokenSequence;
            std::vector<int> expectedSequence = 
                { INT_KEYWD, CHAR_KEYWD, EXTERN_KEYWD, VOID_KEYWD, IF_KEYWD, ELSE_KEYWD,
                FOR_KEYWD, WHILE_KEYWD, RETURN_KEYWD, '(', ')', '{', '}', '[', ']', ';', 
                ',', '=', '+', '-', '*', '/', '!', EQ_RELOP, NEQ_RELOP, LTE_RELOP, LT_RELOP, 
                GTE_RELOP, GT_RELOP, AND_LOGICOP, OR_LOGICOP, ID, ID, ID, ID, INTCON, CHARCON, STRINGCON };
            int currVal;
            while ((currVal = yylex()) != 0) {
                tokenSequence.push_back(currVal);
            }
            THEN("The number of tokens read should match the number expected") {
                REQUIRE(expectedSequence.size() == tokenSequence.size());
            }
            AND_THEN("The sequence of tokens received should match what is expected") {
                REQUIRE(expectedSequence == tokenSequence);
            }
        }
    }

    GIVEN("A valid C-- file containing one of each terminal") {
        yyin = fopen("testfiles/lexer/legal_c_with_all_terminals.c", "r");

        WHEN("yylex is repeatedly called") {
            std::vector<int> tokenSequence;
            std::vector<int> expectedSequence =
            {
                INT_KEYWD, ID, ';', 
                CHAR_KEYWD, ID, ';', 
                CHAR_KEYWD, ID, '[', INTCON, ']', ';', 
                EXTERN_KEYWD, VOID_KEYWD, ID, '(', VOID_KEYWD, ')', ';', 
                INT_KEYWD, ID, '(', VOID_KEYWD, ')', '{', 
                INT_KEYWD, ID, ',', ID, ',', ID, ';',
                INT_KEYWD, ID, '[', INTCON, ']', ';',
                ID, '=', INTCON, ';',
                ID, '=', INTCON, ';',
                IF_KEYWD, '(', ID, LT_RELOP, ID, ')', '{',
                ID, '(', ')', ';',
                '}', ELSE_KEYWD, '{',
                ID, '=', ID, '+', ID, '-', ID, '*', ID, '/', ID, ';',
                ID, '=', '!', ID, ';',
                ID, '=', '-', ID, ';',
                IF_KEYWD, '(', ID, LT_RELOP, ID, OR_LOGICOP, ID, LTE_RELOP, ID, AND_LOGICOP, ID, EQ_RELOP, ID, OR_LOGICOP, ID, GTE_RELOP, ID, AND_LOGICOP, ID, GT_RELOP, ID, ')', '{',
                ID, '(', ')', ';',
                '}',
                '}',
                FOR_KEYWD, '(', ';', ';', ')', '{',
                WHILE_KEYWD, '(', ID, EQ_RELOP, ID, ')', '{',
                ID, '(', ')', ';',
                '}',
                '}',
                ID, '=', CHARCON, ';',
                ID, '=', CHARCON, ';',
                ID, '=', CHARCON, ';',
                ID, '=', STRINGCON, ';',
                RETURN_KEYWD, ID, ';',
                '}'
            };

            int currVal;
            while ((currVal = yylex()) != 0) {
                tokenSequence.push_back(currVal);
            }
            THEN("The number of tokens read should match the number expected") {
                REQUIRE(expectedSequence.size() == tokenSequence.size());
            }
            AND_THEN("The sequence of tokens received should match what is expected") {
                for (int i = 0; i < expectedSequence.size(); i++) {
                    CAPTURE(i);
                    CAPTURE(expectedSequence.size());
                    REQUIRE(expectedSequence.at(i) == tokenSequence.at(i));
                }
            }
        }
    }
}

SCENARIO("Newline and Null char constants don't cause any issues", "[lexer]") {
    GIVEN("A file with newline and null byte character constants") {
        yyin = fopen("testfiles/lexer/newline_null.c", "r");
        WHEN("yylex is called repeatedly") {
            std::vector<char> lexemeSequence;
            std::vector<char> expectedSequence = { '\n', '\0' };

            char expectedChar = '\n';
            int currVal;
            while ((currVal = yylex()) != 0) {
                lexemeSequence.push_back(yylval.charVal);
            }
            THEN("The number of tokens read should match the number expected") {
                REQUIRE(expectedSequence.size() == lexemeSequence.size());
            }
            AND_THEN("The sequence of lexemes received should match what is expected") {
                for (int i = 0; i < expectedSequence.size(); i++) {
                    CAPTURE(i);
                    CAPTURE(expectedSequence.size());
                    REQUIRE(expectedSequence.at(i) == lexemeSequence.at(i));
                }
            }
        }
    }
}