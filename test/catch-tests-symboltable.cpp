#include <string.h>
#include "catch.hpp"
#include "../src/symboltable.h"

SCENARIO("A symbol can be created, modified, and destroyed", "[symboltable]") {
    GIVEN("A standard symbol") {
        Symbol* sym = Symbol_new("myIdentifier", INT_TYPE);

        WHEN("The symbol is first created") {
            THEN("All fields should be initialized as expected") {
                REQUIRE(strcmp(sym->name, "myIdentifier") == 0);
                REQUIRE(sym->type == INT_TYPE);
                REQUIRE(sym->array_numElements == 0);
                REQUIRE(sym->function_arg == NULL);
                REQUIRE(sym->function_returnType == UNASSIGNED_TYPE);
                REQUIRE(sym->nextSymbol == NULL);
            }
            SymbolTable_free(sym, true);
            sym = NULL;
        }

        WHEN("The symbol is modified") {
            Symbol_setType(sym, CHARARR_TYPE);
            Symbol_setNumArrElements(sym, 20);
            Symbol_setArgSymbol(sym, Symbol_new("arg1", INT_TYPE));
            Symbol_setReturnType(sym, INTARR_TYPE);
            THEN("Those modifications should be reflected") {
                REQUIRE(strcmp(sym->name, "myIdentifier") == 0);
                REQUIRE(sym->type == CHARARR_TYPE);
                REQUIRE(sym->array_numElements == 20);
                REQUIRE(sym->function_arg != NULL);
                REQUIRE(SymbolTable_contains(sym->function_arg, "arg1"));
                REQUIRE(strcmp(sym->function_arg->name, "arg1") == 0);
                REQUIRE(sym->function_arg->type == INT_TYPE);
                REQUIRE(sym->function_returnType == INTARR_TYPE);
            }
            SymbolTable_free(sym, true);
            sym = NULL;
        }
    }
}

SCENARIO("A symbol table can be created, modified, and destroyed", "[symboltable]") {
    GIVEN("A new symbol table") {
        Symbol* sym = SymbolTable_new();

        WHEN("A symbol is populated in the table") {
            Symbol* sym2 = Symbol_new("myIdentifier", INT_TYPE);
            SymbolTable_append(sym, sym2);
            THEN("The symbol should be in the table") {
                REQUIRE(SymbolTable_contains(sym, sym2->name));
                REQUIRE(SymbolTable_get(sym, sym2->name) == sym2);
                REQUIRE(SymbolTable_length(sym) == 2);
            }
            SymbolTable_free(sym, true);
            sym = NULL;
            sym2 = NULL;
        }

        // WHEN("An attempt is made to add a duplicate entry") {
        //     Symbol* sym2 = Symbol_new("myIdentifier", INT_TYPE);
        //     SymbolTable_append(sym, sym2);
        //     SymbolTable_append(sym, sym2);

        //     THEN("The duplicate shouldn't have been added") {
        //         REQUIRE(SymbolTable_contains(sym, sym2->name));
        //         REQUIRE(SymbolTable_get(sym, sym2->name));
        //         REQUIRE(SymbolTable_length(sym) == 2);
        //     }
        //     SymbolTable_free(sym, true);
        //     sym = NULL;
        //     sym2 = NULL;
        // }

        WHEN("A second symbol is added") {
            Symbol* sym2 = Symbol_new("myIdentifier", INT_TYPE);
            Symbol* sym3 = Symbol_new("myIdentifier2", CHARARR_TYPE);
            SymbolTable_append(sym, sym2);
            SymbolTable_append(sym, sym3);

            THEN("The new table should reflect that") {
                REQUIRE(SymbolTable_contains(sym, sym3->name));
                REQUIRE(SymbolTable_get(sym, sym3->name));
                REQUIRE(SymbolTable_length(sym) == 3);
            }
            SymbolTable_free(sym, true);
            sym = NULL;
            sym2 = NULL;
            sym3 = NULL;
        }

        WHEN("In a [a] -> [b] -> [c] list, [a] is removed") {
            Symbol* sym2 = Symbol_new("myIdentifier", INT_TYPE);
            Symbol* sym3 = Symbol_new("myIdentifier2", CHARARR_TYPE);
            SymbolTable_append(sym, sym2);
            SymbolTable_append(sym, sym3);

            sym = SymbolTable_remove(sym, sym->name);
            THEN("The new table should reflect that") {
                REQUIRE(sym == sym2);
                REQUIRE(SymbolTable_contains(sym, sym2->name) == true);
                REQUIRE(SymbolTable_contains(sym, sym3->name) == true);
                REQUIRE(SymbolTable_get(sym, sym2->name) == sym2);
                REQUIRE(SymbolTable_get(sym, sym3->name) == sym3);
                REQUIRE(SymbolTable_get(sym, sym->name)->nextSymbol == sym3);
                REQUIRE(SymbolTable_length(sym) == 2);
            }
        }
        
        WHEN("In a [a] -> [b] -> [c] list, [b] is removed") {
            Symbol* sym2 = Symbol_new("myIdentifier", INT_TYPE);
            Symbol* sym3 = Symbol_new("myIdentifier2", CHARARR_TYPE);
            SymbolTable_append(sym, sym2);
            SymbolTable_append(sym, sym3);

            sym = SymbolTable_remove(sym, sym2->name);
            THEN("The new table should reflect that") {
                REQUIRE(SymbolTable_contains(sym, sym2->name) == false);
                REQUIRE(SymbolTable_contains(sym, sym3->name) == true);
                /* TODO: Why doesn't this segfault? sym2 should have been freed right? */
                REQUIRE(SymbolTable_get(sym, sym2->name) == NULL);
                REQUIRE(SymbolTable_get(sym, sym3->name) == sym3);
                REQUIRE(SymbolTable_get(sym, sym->name)->nextSymbol == sym3);
                REQUIRE(SymbolTable_length(sym) == 2);
            }
        }

        WHEN("In a [a] -> [b] -> [c] list, [c] is removed") {
            Symbol* sym2 = Symbol_new("myIdentifier", INT_TYPE);
            Symbol* sym3 = Symbol_new("myIdentifier2", CHARARR_TYPE);
            SymbolTable_append(sym, sym2);
            SymbolTable_append(sym, sym3);

            sym = SymbolTable_remove(sym, sym3->name);
            THEN("The new table should reflect that") {
                REQUIRE(SymbolTable_contains(sym, sym2->name) == true);
                REQUIRE(SymbolTable_contains(sym, sym3->name) == false);
                REQUIRE(SymbolTable_get(sym, sym2->name) == sym2);
                /* TODO: Why doesn't this segfault? sym3 should have been freed right? */
                REQUIRE(SymbolTable_get(sym, sym3->name) == NULL);
                REQUIRE(SymbolTable_get(sym, sym->name)->nextSymbol == sym2);
                REQUIRE(SymbolTable_length(sym) == 2);
            }
        }

        WHEN("The table is cleared") {
            SymbolTable_free(sym, true);
            sym = NULL;

            THEN("The table should be empty") {
                REQUIRE(SymbolTable_length(sym) == 0);
            }
        }
    }
}
