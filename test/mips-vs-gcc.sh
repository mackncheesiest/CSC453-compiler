#!/bin/bash

MYPROG=../src/target/compile
CC=gcc

file=$1

$MYPROG < $file >mipsAsm 2>/dev/null
myStatusCode=$?
spim -f mipsAsm | sed '1,5d' &>mipsResult

cat $file | sed 's/extern void print_int(.*);//g' | sed 's/extern void print_string(.*);//g' > ccFile.c
$CC -include 'stdio.h' -D'print_int(x)=printf("%d",(x))' -D'print_string(x)=printf("%s",(x))' ccFile.c -o x86exe 2>/dev/null
./x86exe > x86result

diff mipsResult x86result

rm mipsAsm mipsResult ccFile.c x86exe x86result