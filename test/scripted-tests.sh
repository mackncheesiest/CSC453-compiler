#!/bin/bash

TESTDIR=./testfiles
MYPROG=../src/target/compile
CC=gcc

NUMRUN=0
NUMPASSED=0

declare -A testFolders
if [ $# -gt 0 ]; then
    for arg in $@
    do
        testFolders["$TESTDIR/$arg"]=1
    done
else
    temp=`find $TESTDIR -type d -regex '.*/\(valid\|invalid\)$'`
    for folder in ${temp[@]}
    do
        testFolders["${folder%/*}"]=1
    done
fi

tempdir=$(mktemp -d "${TMPDIR:-/tmp/}$(basename $0).XXXXXXXXXXXX")
trap 'rm -rf -- "$tempdir"' INT TERM HUP EXIT

for folder in ${!testFolders[@]}
do
    if [ ! -d "$folder" ]; then
        echo "Skipping test execution in nonexistant directory $folder"
        continue
    fi

    if [[ "$folder" == *"codegen"* ]]; then
        
        echo "Found a codegen folder $folder, so all files must be valid. Executing GCC comparison tests"
        
        if [ ! -d "${folder}/valid" ]; then
            echo "$folder has no 'valid' folder filled with valid testfiles"
        else
            echo "Validating $folder test data"
            for file in $folder/valid/*.c 
            do
                failed=0
                echo "*******  TESTING: ${file%.*}  *******"

                $MYPROG < $file >$tempdir/mipsAsm 2>/dev/null
                myStatusCode=$?
                spim -f $tempdir/mipsAsm | sed '1,5d' &> $tempdir/mipsResult

                cat $file | sed 's/extern void print_int.*;//g' | sed 's/extern void print_string.*;//g' > $tempdir/ccFile.c
                $CC -include 'stdio.h' -D'print_int(x)=printf("%d",(x))' -D'print_string(x)=printf("%s",(x))' $tempdir/ccFile.c -o $tempdir/x86exe 2>/dev/null
                $tempdir/x86exe > $tempdir/x86result
                
                if [ $myStatusCode -eq 0 ]; then
                    echo "Status code check passed! $myStatusCode (mine) == 0 (expected)"
                else
                    echo "Status code check didn't pass! $myStatusCode (mine) != 0 (expected)"
                    failed=1
                fi

                if diff -q $tempdir/mipsResult $tempdir/x86result; then
                    echo "MIPS execution results matched with expected GCC results"
                else
                    echo "MIPS execution results differed from expected GCC results"
                    failed=1
                fi

                if [ $failed -eq 0 ]; then
                    echo PASSED
                    ((NUMPASSED++))
                else
                    echo FAILED
                fi
                echo
                ((NUMRUN++))
            done
        fi

    else # End $folder == *"codegen"*

        echo "Executing status code comparison tests in $folder"
        if [ ! -d "${folder}/valid" ]; then
            echo "$folder has no 'valid' folder filled with valid testfiles"
        else
            echo "Validating $folder test data"
            for file in $folder/valid/*.c 
            do
                failed=0
                echo "*******  TESTING: ${file%.*}  *******"

                $MYPROG < $file >/dev/null 2>&1
                myStatusCode=$?
    
                if [ $myStatusCode -eq 0 ]; then
                    echo "Status code matches! $myStatusCode (mine) == 0 (theirs)"
                else
                    echo "Status code doesn't match! $myStatusCode (mine) != 0 (theirs)"
                    failed=1
                fi

                if [ $failed -eq 0 ]; then
                    echo PASSED
                    ((NUMPASSED++))
                else
                    echo FAILED
                fi
                echo
                ((NUMRUN++))
            done
        fi

        if [ ! -d "${folder}/invalid" ]; then
            echo "$folder has no 'invalid' folder filled with invalid testfiles"
        else 
            for file in $folder/invalid/*.c
            do
                failed=0
                echo "*******  TESTING: ${file%.*}  *******"

                $MYPROG < $file >/dev/null 2>stderr_out.txt
                myStatusCode=$?
    
                if [ $myStatusCode -eq 1 ] && [ -s stderr_out.txt ]; then
                    echo "Status code matches! $myStatusCode (mine) == 1 (theirs)"
                else
                    echo "Status code doesn't match! $myStatusCode (mine) != 1 (theirs)"
                    failed=1
                fi

                if [ -s stderr_out.txt ]; then
                    echo "stderr_out.txt has stuff in it! (Yay!)"
                else
                    echo "stderr_out.txt has no stuff in it! (Boo!)"
                    failed=1
                fi

                rm stderr_out.txt
                if [ $failed -eq 0 ]; then
                    echo PASSED
                    ((NUMPASSED++))
                else
                    echo FAILED
                fi
                echo
                ((NUMRUN++))
            done
        fi
        
    fi
done

echo
echo $NUMPASSED OUT OF $NUMRUN TESTS PASSED!
if [ $NUMPASSED -eq $NUMRUN ]; then
    exit 0
else
    exit 1
fi
