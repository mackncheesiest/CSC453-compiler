extern void print_int(int x);
extern void print_string(char s[]);

int x[10];
char y[20];

void main(void) {
  int x2[10];
  char y2[20];
  x[2] = 5;
  y[10] = 'a';
  x2[9] = 11;
  y2[15] = 'b';
  print_int(x[2]);
  print_string("\n");
  print_int(y[10]);
  print_string("\n");
  print_int(x2[9]);
  print_int(y2[15]);
}
