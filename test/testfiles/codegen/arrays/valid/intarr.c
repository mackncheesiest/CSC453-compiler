extern void print_int(int x);

void print_int_arr(int x[], int length) {
    int i;
    for (i = 0; i < length; i = i + 1) {
        print_int(x[i]);
    }
}

int global[2];

void main(void) {
    int local[3];
    local[0] = 1;
    local[1] = 2;
    local[2] = 3;
    global[0] = 4;
    global[1] = 5;
    print_int_arr(local, 3);
    print_int_arr(global, 2);
}