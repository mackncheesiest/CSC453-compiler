extern void print_int(int x);
extern void print_string(char arr[]);

void nl(void) {
  print_string("\n");
}

int w[20];
char s[14];

void func2(void);

void main(void) {
  int i; char s[14], alpha[28];
  for (i = 0; i < 20; i = i + 1) {
    w[i] = i*i;
  }
  for (i = 19; i >= 0; i = i - 1) {
    print_int(w[i]);
    nl();
  }

  for (i = 1; i < 20; i = i + 1) {
    w[i] = 2*w[i-1] + w[i]/2 - 10;
  }
  for (i = 19; i >= 0; i = i - 1) {
    print_int(w[i]);
    nl();
  }
  
  s[0] = 'w';
  s[1] = 'o';
  s[2] = 'w';
  s[3] = '\n';
  s[4] = '\0';
  
  func2();
  print_string(s);
  
  print_int(s[0]); nl();
  print_int(s[1]); nl();
  print_int(s[2]); nl();

  for (i = 0; i < 26; i = i + 1) {
    alpha[i] = 'a' + i;
  }
  alpha[26] = '\n';
  alpha[27] = '\0';
  print_string(alpha);
}

void func2(void) {
  s[0] = 'c';
  s[1] = 'o';
  s[2] = 'o';
  s[3] = 'l';
  s[4] = '\n';
  s[5] = '\0';
  print_string(s);
}