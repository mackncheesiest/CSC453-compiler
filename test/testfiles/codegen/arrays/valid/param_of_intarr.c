extern void print_int(int x);
extern void print_string(char arr[]);

void print_int_arr(int arr[], int length) {
    int i;
    for (i = 0; i < length; i = i + 1) {
        if (i > 0) {
            print_string(", ");
        }
        print_int(arr[i]);
    }
    print_string("\n");
}

int globalArr[20];

void main(void) {
    int localArr[15];
    int i;
    for (i = 0; i < 15; i = i + 1) {
        localArr[i] = i+1;

    }
    for (i = 0; i < 20; i = i + 1) {
        globalArr[i] = i-1;
    }
    print_string("localArr:\n");
    print_string("inside main:\n");
    for (i = 0; i < 15; i = i + 1) {
        if (i > 0) {
            print_string(", ");
        }
        print_int(localArr[i]);
    }
    print_string("\n");
    print_string("from print_int_arr:\n");
    print_int_arr(localArr, 15);
    print_string("globalArr:\n");
    print_string("inside main:\n");
    for (i = 0; i < 20; i = i + 1) {
        if (i > 0) {
            print_string(", ");
        }
        print_int(globalArr[i]);
    }
    print_string("\n");
    print_string("from print_int_arr:\n");
    print_int_arr(globalArr, 20);
}