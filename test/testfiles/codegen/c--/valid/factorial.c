extern void print_int(int x);
extern void print_string(char x[]);

int fact(int x) {
    if (x < 0) {
        return 1;
    } else {
        return x * fact(x-1);
    }
}

int main(void) {
    print_int(fact(7));
    print_string("\n");
    return 0;
}