extern void print_int(int x);
extern void print_string(char arr[]);

int checkPrime(int x);

int main(void) {
    int i, size, currentPrime, numPrimes;

    size = 100;
    currentPrime = 2;
    numPrimes = 1;

    while (currentPrime < size) {
        i = currentPrime + 1;
        if (checkPrime(i) == 1) {
            print_string("Found a prime:");
            print_int(i);
            print_string("\n");
            numPrimes = numPrimes + 1;
        }
        currentPrime = currentPrime + 1;
    }
    print_string("Found a total of ");
    print_int(numPrimes);
    print_string(" primes\n");
    return 0;
}

int checkPrime(int num) {
    int i, temp;
    for (i = 2; i < num; i = i + 1) {
        temp = num / i;
        if (i * temp == num) {
            return -1;
        }
    }
    return 1;
}