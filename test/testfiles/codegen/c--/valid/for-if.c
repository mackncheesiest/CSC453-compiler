extern void print_int(int x);
extern void print_string(char str[]);

void main(void) {
    int i;
    print_string("test");
    for (i = 0; i < 10; i = i + 1) {
        if (i > 0) {
            print_string(", ");
        }
        print_int(i);
    }
    print_string("\n");
    for (i = 0; i < 15; i = i + 1) {
        if (i > 0) {
            print_string(", ");
        }
        print_int(i);
    }
    print_string("\n");
}