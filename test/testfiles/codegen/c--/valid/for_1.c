extern void print_int(int x);
extern void print_string(char arr[]);

void pi(int x) {
  print_int(x);    
}

void pn(void) {
  print_string("\n");
}

int main(void) {
  int i;
  for (i = -1; i > -2 && i <= 15; i = i + 3) {
    pi(i);
    pn();
  } 
  return i;
}
