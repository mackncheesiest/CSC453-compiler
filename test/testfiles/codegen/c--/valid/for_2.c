extern void print_int(int x);
extern void print_string(char arr[]);

void pi(int x) {
  print_int(x);    
}

void pn(void) {
  print_string("\n");
}

void f0(void) {
  int i;
  for (i = 10; i < 5; i = i + 1) { 
    print_string("A lonely life, never printed to the screen\n");
  }
  if (i == 10) {
    print_string("I am printed though\n");
  }
}

void f1(void) {
  int i;
  print_string("f1\n");
  i = 0;
  for (;;) {
    pi(i);
    pn();
    i = i + 1;
    if (i > 4) return; 
  }
}

void f2(void) {
  int i;
  print_string("f2\n");
  for (i = 10; ;) {
    pi(i);
    pn();
    i = i + 2;
    if (!(i < 21)) return;
  }
}

void f3(void) {
  int i;
  i = 20;
  print_string("f3\n");
  for (; i > -10 ; ) {
    pi(i);
    pn();
    i = i - 3;
  }
}

void f4(void) {
  int i;
  i = -20;
  print_string("f4\n");
  for (; ; i = i + 7) {
    pi(i);
    pn();
    if (!!(i >= 50)) { return; }
  }
}

void f5(void) {
  int i;
  print_string("f5\n");
  for (i = 0; i < 15; i = i + 1) {
    if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7) {
      pi(i);
      pn();
    }
    if (i == 8 || i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14) {
      pi(-i);
      pn();
    }
  }
  pi(i);
  pn();
}

void f6(void) {
  int i, j, k, limit;
  print_string("f6\n");
  limit = 10;
  for (i = 0; i < limit; i = i + 1) {
    for (j = i; j < limit + i; j = j + 2) {
      for (k = j; k < limit + j; k = k + 3) {
        pi(i); print_string(", "); pi(j); print_string(", "); pi(k); pn();
        pi(i * j * k);
        pn();
      }
    }
  }
}

void f7(void) {
  int i, j, k, l, limit;
  print_string("f7\n");
  limit = 10;
  for (i = 0; i < limit; i = i + 1) {
    for (j = 0; j < limit; j = j + 1) {
      for (k = 0; k < limit; k = k + 1) {
        for (l = 0; l < limit; l = l + 1) {
          if (i == j && j == k && k == l && i == l) {
            pi(i); print_string(", "); pi(j); print_string(", "); pi(k); print_string(", "); pi(l); pn();
            if (l != 0) {
              pi(i + j - k / l);
            } else {
              pi(i + j - k * l);
            }
            pn();
          } else {
            /* Wow this branch is actually empty */
          }
        }
      }
    }
  }
}

int main(void) {
  int i;
  f0();
  f1();
  f2();
  f3();
  f4();
  f5();
  f6();
  f7();
  return 0;
}
