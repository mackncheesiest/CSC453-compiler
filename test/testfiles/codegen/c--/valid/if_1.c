extern void print_string(char str[]);
extern void print_int(int x);

int x, y;

int main(void) {
  x = 4;
  y = 5;
  if (x == y) {
    print_int(y);
  } else {
    print_int(-y);
  }
  print_string("\n");
  print_int(123456);
  return 0;
}
