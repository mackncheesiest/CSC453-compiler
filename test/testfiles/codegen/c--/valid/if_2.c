extern void print_int(int x);
extern void print_string(char arr[]);

void pi(int x) {
  print_int(x);    
}

void pn(void) {
  print_string("\n");
}

void f1(void) {
    int a, b, c, d;
    print_string("f1\n");
    a = -1; b = 1; c = 2; d = 5;
    if (a < 0) {
        b = a;
        if (b + a > c - d) {
            d = a + b;
            if (a*b*c*d < b*c*a*d || a+b == b+a) {
                a = c;
                if (d >= a+b-c && d-7 < 0) {
                    d = 2*a + 3*b + 4*c + 5*(d/2);
                } else {
                    print_string("made it four if statements deep\n");
                    d = a/2 - b/3 - c/4 - 2*(d/5) + 1022; 
                }
            } else {
                print_string("made it three if statements deep\n");
                a = d;
            }
        } else {
            print_string("made it two if statements deep\n");
            d = a - b;
        }
    } else {
        print_string("wow we failed on the first if statement\n");
    }
    pi(a); print_string(", "); pi(b); print_string(", "); pi(c); print_string(", "); pi(d); pn();
}

int main(void) {
    f1();
    return 0;
}