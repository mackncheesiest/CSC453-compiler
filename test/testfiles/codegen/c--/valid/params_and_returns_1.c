extern void print_int(int x);
extern void print_string(char str[]);

int func2(int x, int y, char z);

int main(void) {
  int x, y, z;
  x = func2(0, 1, 'a');
  y = x * 2;
  z = y / 3;
  print_int(x);
  print_string("\n");
  return (x + y + z)/3;
}

int func2(int x, int y, char z) {
  return x * y - z;
}
