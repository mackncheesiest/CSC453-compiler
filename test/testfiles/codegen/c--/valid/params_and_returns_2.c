int x, w;
char y, z;

extern void print_int(int x);
extern void print_string(char c[]);

int func2(int x, int y);
int func3(char x, int y, char z);
int func4(int x, int y, int z, int w);

int main(void) {
  char local;
  int local2;
  print_string("hello\n");
  w = func2(x, 5);
  print_string("look, after calling func2, the global value of x is unchanged\n");
  print_int(x);
  print_string("\n");
  print_string("look, w actually stores the value returned from func2\n");
  print_int(w);
  print_string("\n");
  local = func3('b', 1, ' ');
  local2 = func3('c', 2, ' ') + func2(x, 20);
  print_string("This be equivalent to an uppercase A, I think...\n");
  print_int(local);
  print_string("\n");
  print_string("This should be the same big number from before except plus 65ish\n");
  print_int(local2);
  print_int(func4(1, 2, 3, 4));
  return w;
}


int func2(int x, int y) {
  int local1, local2, local3;
  print_string("look, i can do arithmetic\n");
  x = -7 + 8 + 9 * 10 - 11 / 11; /* -7 + 8 + 90 - 1 == 90 */
  print_int(x);
  print_string("\n");
  print_string("look, i can perform operations with constants larger than the MIPS immediate field size of 2^16\n");
  x = 131072 + y;
  local1 = x + 1;
  local2 = local1 + 2;
  local3 = local2 * (1 / 5);
  x = x + local1 + local2 + local3;
  print_int(local1); print_string("\n");
  print_int(local2); print_string("\n");
  print_int(local3); print_string("\n");
  print_string("yeesh, x wrapped around\n");
  print_int(x); print_string("\n");
  return x;
}

int func3(char x, int y, char z2) {
  int func3local;
  print_string("wow, this function has 3 arguments\n");
  func3local = 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2;
  print_string("this number is representable\n");
  print_int(func3local); print_string("\n");
  func3local = 2 * func3local;
  print_string("woah we reached 2^32!\n");
  print_int(func3local); print_string("\n");
  print_string("what a mix of types\n");
  return x - y - z2;
}

int func4(int x, int y, int z, int w) {
  return func3(x, y, z);
}