/*
Ported from ECE 175 Lab 3 P2
*/

extern void print_int(int x);
extern void print_string(char arr[]);

int main(void) {
    int pHeight, pWidth, i; /* pHeight = height of pyramid, pWidth = width of pyramid, i = loop variable */

    pWidth = 0;
    pHeight = 10;
    while (pWidth <= pHeight) {
        for (i = 1; i <= pHeight - pWidth; i = i + 1) {
            print_string(" ");
        }
        for (i = pHeight - i; i >= 0; i = i - 1) {
            print_string("*");
        }
        print_string("\n");
        pWidth = pWidth + 1;
    }
    pWidth = pWidth - 2;
    while (pWidth >= 0) {
        for (i = 1; i <= pHeight - pWidth; i = i + 1) {
            print_string(" ");
        }
        for (i = pHeight - i; i >= 0; i = i - 1) {
            print_string("*");
        }
        print_string("\n");
        pWidth = pWidth - 1;
    }
    return 0;
}