/* assignment: global to global */

int x;
int y;

extern void print_int (int n);
extern void print_string (char s[]);

void main(void)
{
  x = 3142;
  y = x;
  print_int(y);
  print_string("\n");
}
