/* several global-to-global assignments */

int x, y, z;

extern void print_int (int n);
extern void print_string (char s[]);

void main(void)
{
  x = 111;
  y = x;
  z = y;
  print_int(z);
  print_string("\n");

  z = 222;
  x = z;
  y = x;
  print_int(y);
  print_string("\n");

  y = 333;
  x = y;
  z = x;
  print_int(z);
  print_string("\n");
}
