/* a single local-to-local assignment */

extern void print_int (int n);
extern void print_string (char s[]);

void main(void)
{
  int x;
  int y;

  x = 4321;
  y = x;
  print_int(y);
  print_string("\n");
}
