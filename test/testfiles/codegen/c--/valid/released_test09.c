/* several local-to-local assignments 1 */

extern void print_int (int n);
extern void print_string (char s[]);

void main(void)
{
  int x, y, z;
  int u;

  /*
   * A sequence of assignments followed by a sequence of function calls
   */
  x = 1111;
  y = 2222;
  z = 3333;
  u = 4444;

  print_int(u);
  print_string("\n");

  print_int(x);
  print_string("\n");

  print_int(y);
  print_string("\n");

  print_int(z);
  print_string("\n");

  /*
   * A sequence of assignments interspersed with function calls
   */
  x = 1111;
  print_int(x);
  print_string("\n");

  y = 2222;
  print_int(y);
  print_string("\n");

  z = 3333;
  print_int(z);
  print_string("\n");

  u = 4444;
  print_int(u);
  print_string("\n");
}
