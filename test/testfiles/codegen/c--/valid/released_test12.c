/* assignments to several global chars */

char x, y, z;

extern void print_int (int n);
extern void print_string (char s[]);

void main(void)
{
  /*
   * A sequence of assignments followed by a sequence of function calls
   */
  x = 'A';
  y = 'B';
  z = 'C';
  print_int(x);
  print_string("\n");
  print_int(y);
  print_string("\n");
  print_int(z);
  print_string("\n");

  /*
   * A sequence of assignments interspersed with function calls
   */
  x = 'A';
  print_int(x);
  print_string("\n");

  y = 'B';
  print_int(y);
  print_string("\n");

  z = 'C';
  print_int(z);
  print_string("\n");

}
