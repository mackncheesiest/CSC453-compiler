/* more complex while loops */

int i, x0, x1, x2, x3, x4, x5;
int res;

extern void print_int (int n);
extern void print_string (char s[]);

void main(void)
{
  i = 1;
  x1 = 1;
  x2 = 10;
  x3 = 100;

  res = 123;
  while (x1 < x2 && (x2 != x3 || x3 / x2 > x1)) {
    res = res + i;
    i = i * 2;
    x1 = x1 + 1;
    x3 = x3 - 5;
  }
  print_int(res);
  print_string("\n");
}
