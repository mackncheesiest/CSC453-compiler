/*
Ported from ECE 175 Lab 3 P1
*/

extern void print_int(int x);
extern void print_string(char arr[]);

int main(void) {
    int sum, i, temp;

    sum = 0;

    for (i = 1; i <= 100; i = i + 1) {
        /* C-- equivalent of a modulus operation */
        temp = i / 8;
        if (8 * temp == i) {
            sum = sum + i;
        }
    }
    print_string("The sum of all positive numbers less than 100 divisible by 8 is: ");
    print_int(sum);
    print_string("\n");

    return 0;
}