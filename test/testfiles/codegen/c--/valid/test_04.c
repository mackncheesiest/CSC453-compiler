extern void print_int(int a);
extern void print_string(char a[]);
void nl(void){print_string("\n");}
void sub(int a, int b);
int a,b;
void main(void){
    a = 5;
    b = 1;
    sub(a,b);
    sub(b,a);
    sub(100,99);
    sub(100 + 20, 20 + 100);
    sub(100 - 80, 99 - 79);
    sub(2 + 2 + 2 + 2 + 2 - 10 + 20, 10);   
}

void sub(int a, int b){
    print_int(a - b);
    nl();
}
