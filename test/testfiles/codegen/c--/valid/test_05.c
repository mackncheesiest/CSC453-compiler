extern void print_int(int a);
extern void print_string(char a[]);
void nl(void){print_string("\n");}
void mult(int a, int b);
void print(int a);
int a,b;

void main(void){
    a = 5;
    b = 1;
    mult(a,b);
    mult(b,a);
    mult(100,99);
    mult(100 + 20, 20 + 100);
    mult(100 - 80, 99 - 79);
    mult(2 + 2 + 2 + 2 + 2 - 10 + 20, 10);  
    print(1 + 2 * 5); 
    print( (1+2) * 5);
    /* 48 */
    print( ( 1 + ( 2 * (5 + 5) + 3 * 9)));
}
void print(int a){
    print_int(a);
    nl();
}
void mult(int a, int b){
    print_int(a * b);
    nl();
}

