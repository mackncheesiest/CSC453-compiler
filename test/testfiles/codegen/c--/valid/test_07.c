extern void print_int(int a);
extern void print_string(char a[]);
void p(int a);

void main(void){
    int a,b,c;
    a = -1;
    b = 2;
    c = 3;
    p(a);   
    p(-a);
    p(-a + a);
    p(a + -a);
    /*-16*/
    p(-1 * -2 + -3 * (3 - -3)); 
}

void p(int a){
    print_int(a);
    print_string("\n");
}
