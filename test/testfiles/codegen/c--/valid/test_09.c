extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}

void main(void){
    int a;
    int b;

    a = 1;
    b = 2;
    if(a == b){
        p(66);
    }
    b = 1;
    if(a == b){
        p(100);
    }
    if(1 == 1){
        p(1);
        if(2 == 2){
            p(2);   
            if( 3 == 3){
                p(3);
            }
            else{
                p(-1000);
            }
        }
        else{
            p(-1000);
        }
    
    }
    else{
        p(-1000);
    }
}

