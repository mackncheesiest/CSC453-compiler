extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}

void main(void){
    int a;
    int b;

    a = 1;
    b = 2;
    if(a == b){
        p(66);
    }
    b = 1;
    if(a == b){
        p(100);
    }
    if(1 < 2){
        p(1);
    }
    if(1 <= 2){
        p(2);
    }
    if(2 > 1){
        p(3);
    }
    if(2 >= 1){
        p(4);
    }
    if(2 >= 2){
        p(5);
    }
    if(2 <= 2){
        p(6);
    }
    if(2 != 1){
        p(7);
    } 
    
}

