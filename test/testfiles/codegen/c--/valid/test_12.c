extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}

void main(void){
    int a;
    int b;

    a = 1;
    b = 2;
    if(a <= b && a < b){
        p(1);
    }
    else{
        p(-1000);
    }
    if( a < b && b < a){
        p(-2000);
    }
    else{
        p(2);
    }   
    if( b < a || a < b ){
        p(3);
    }
    else{
        p(-3000);
    }
    if( a < b || a == b){
        p(4);
    }
    else{
        p(-4000);
    }
    if(a == b || b == a){
        p(-5000);
    }
    else{
        p(5);  
    }
    if( !(b < a)){
        p(6);
    }
    else{
        p(-6000);
    }
    if( !(b < a) && (b < a || a < b)){
        p(7);
    }
    else{
        p(-7000);
    }
    if( a < b && b > a && a == a && b == 2 && (a == 1000 || a == 2000 || a == 3000 || a == 1)){
        p(8);
    }
    else{
        p(-8000);
    }
}

