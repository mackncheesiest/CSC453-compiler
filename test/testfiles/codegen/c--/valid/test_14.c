extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}


void main(void){
    int a;
    for(a = 0; a < 10; a = a+1){
        p(a);
    }
    for(; a < 20; a = a+1){
        p(a);
    }
    for(a = 20; a < 30;){
        p(a);
        a = a + 1;
    }
    for(a = 30; ; a = a + 1){  
        p(a);
        if(a == 50){
            return;
        }
    }
}
    
