extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}

int f(int a, int b, int c, int d, int e, int f, int g){
    return a + b + c + d + e + f + g;
}
int mult(int a, int b){
    return a * b;
}
int add(int a, int b){
    return a + b;
}
int sub(int a, int b){
    return a - b;
}
void main(void){
    int a;
    /* 110 */
    p(f(1 , 1, 1, 1, 1 + 1 + 1 + 1 + 1, 10 * 10, 1));    
                   /* (5 * (10 + 10 - 5)) + 50 = 125 */
    a = add( mult( 5, add( 10, sub( 10, add( 5, 0)) ) ), mult(5,10));
    p(a);
}
    
