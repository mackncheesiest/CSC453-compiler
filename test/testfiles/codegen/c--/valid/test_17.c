extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}

int x;
char y;

int z;
int f(int a);
int g(int a);
int h(int a);
int j(int a);
int l(int a);

void main(void){
    int a;
    char b;
    int c;
    char d;
    
    a = 5;
    /*print -20*/
    print_int(f(a));
    print_string("\n");
}

/*return -45*/
int f(int a){
    int b;
    b = 5 * g(a);
    p(b);
    return b;
}

/*return -9*/
int g(int a){
    int b;
    b = a - 2 * h(a);
    p(b);
    return b;
}

/*return 7*/
int h(int a){
    int b;
    b = a + j(a);
    p(b);
    return b;
}

/*return 2*/
int j (int a){
    int b;
    b = l(a) / a;
    p(b);
    return b;
}

/*Return 10*/
int l(int a){
    int b;
    b = 2 * a;
    p(b);
    return b;
}

