extern void print_int(int a);
extern void print_string(char a[]);
void p(int a){print_int(a); print_string("\n");}

int fib(int n){
    if(n <= 1){
        return n;
    }
    return fib(n-1) + fib(n-2);
}

void main(void){
    p(fib(10));
}
