extern void print_int(int a);
extern void print_string(char b[]);
void p(int a){print_int(a); print_string("\n");}

void main(void){
    int i;
    /* Should assign i val of 100 and skip for loop entirely*/
    for(i = 100; i < 5; i = i + 1){
        p(-1000);
    }
    /* Should print */
    if(i == 100){
        p(1);
    }
    
    /*Should print exactly once */
    for(; i < 200; i = i + 100){
        p(2);
    }
    
    if(i != 200 ){
        p(-2000);
    }
    else{
        p(3);
    }
    
    /* Should print 4 through 10*/
    for(i = 4; i <= 10; i = i + 1){
        p(i);
    }
    /*Set i to 10*/
    i = i - 1;
    
    /*print 10 thru 0 backwards*/
    for(; i >= 0; i = i - 1){
        p(i);
    }
}

