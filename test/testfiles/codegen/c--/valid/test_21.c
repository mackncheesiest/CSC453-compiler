extern void print_int(int a);
extern void print_string(char b[]);
void p(int a){print_int(a); print_string("\n");}

char convertInt(int a){return a;}
int convertChar(char a){return a;}
/*
    Let's compare some chars
*/
void main(void){
    int a,b;
    char c,d;
    a = 48;
    b = 49;
    c = '0';
    d = '1';

    if(a == c){
        p(1);
    }else{
        p(-1000);
    }
    if(b == d){
        p(2);
    }else{
        p(-2000);
    }
    if(a == convertInt(a)){
        p(3);
    }
    else{
        print_string("Failed: a = ");
        p(a);
        print_string(" conv = ");
        p(convertInt(a));
        p(-3000);
    }    
   if( a == convertInt(convertChar(c))){
        p(4);
    }
    else{
        p(-4000);
    }    

    if( 1 == convertInt(convertChar(convertInt(convertChar(convertInt(convertChar(convertInt(convertChar(convertInt(1)))))))))){
        p(5);
    }
    else{
        p(-5000);
    }
    if(c == convertChar(c)){
        p(6);
    }
    else{
        p(-6000);
    }
    if(c == convertChar('0')){
        p(7);
    }
    else{
        p(-7000);
    }
    /* Arithmetic tests */
    if( b + b == d + d){
        p(8);
    }
    else{
        p(-8000);
    }
    if(a + d == b + c){
        p(9);
    }
    else{
        p(-9000);
    }
    if(d + a == c + b){
        p(10);
    }
    else{
        p(-10000);
    }
}

