extern void print_int(int x);
extern void print_string(char arr[]);

int x;

int main(void) {
  x = 0;
  while (x < 10) {
    print_int(x);
    print_string("\n");
    x = x + 1;
  }
  return x; 
}
