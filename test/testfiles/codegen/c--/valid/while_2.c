extern void print_int(int x);
extern void print_string(char arr[]);

void pi(int x) {
  print_int(x);    
}

void pn(void) {
  print_string("\n");
}

void pin(int x) {
  pi(x);
  pn();
}

void f1(void) {
  int i;
  print_string("f1\n");
  i = 0;
  while (1 == 1) {
    pin(i);
    i = i + 1;
    if (i > 4) return;
  }
}

void f2(void) {
  int i;
  print_string("f2\n");
  i = 10;
  while (2 > 0 && -1 <= 6) {
    pi(i);
    pn();
    i = i + 2;
    if (!(i < 21)) return;
  }
}

void f3(void) {
  int i;
  i = 20;
  print_string("f3\n");
  while (i > -10) {
    pi(i);
    pn();
    i = i - 3;
  }
}

void f4(void) {
  int i;
  print_string("f4\n");
  while (0 == 1 || 1 < 2) {
    pi(i);
    pn();
    i = i + 7;
    if (!!(i >= 50)) { return; }
  }
}

void f5(void) {
  int i;
  print_string("f5\n");
  i = 0;
  while (i < 15) {
    if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7) {
      pi(i);
      pn();
    }
    if (i == 8 || i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14) {
      pi(-i);
      pn();
    }
    i = i + 1;
  }
}

void f6(void) {
  int i, j, k, limit;
  print_string("f6\n");
  i = 0;
  limit = 10;
  while (i < limit) {
    j = i;
    while (j < limit) {
      k = j;
      while (k < limit) {
        pi(i); print_string(", "); pi(j); print_string(", "); pi(k); pn();
        pi(i * j * k);
        pn();
        k = k + 3;
      }
      j = j + 2;
    }
    i = i + 1;
  }
}

void f7(void) {
  int i, j, k, l, limit;
  print_string("f7\n");
  limit = 10;
  i = 0;
  while (i < limit) {
    j = 0;
    while (j < limit) {
      k = 0;
      while (k < limit) {
        l = 0;
        while (l < limit) {
          if (i == j && j == k && k == l && i == l) {
            pi(i); print_string(", "); pi(j); print_string(", "); pi(k); print_string(", "); pi(l); pn();
            if (l != 0) {
              pi(i + j - k / l);
            } else {
              pi(i + j - k * l);
            }
            pn();
          } else {
            /* Still empty */
          }
          l = l + 1;
        }
        k = k + 1;
      }
      j = j + 1;
    }
    i = i + 1;
  }
}

int main(void) {
f1();
f2();
f3();
f4();
f5();
f6();
f7();
return 0;
}