/*
    Output should be equiv to printf("1\n2\n3\n4\n);
*/
extern void print_int(int a);
extern void print_string(char b[]);


void main(void){
    int a,b,c,d;
    a = 1;
    b = 2;
    c = 3;
    d = 4;
    print_int(a);
    print_int(b);
    print_int(c);
    print_int(d);
    print_string("\n");
}


