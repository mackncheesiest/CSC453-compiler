/*
    Output should be equiv to printf("42\n");
*/
extern void print_string(char a[]);
extern void print_int(int a);
void f(int a);
void f2(int a);
void f3(int a);
void f4(int a);
int b;
void main(void){
    int a;
    b = 42;
    a = b;
    f(a);

}
void f(int a){
    f2(a);
}
void f2(int a){
    f3(a);
}
void f3(int a){
    f4(a);
}
void f4(int a){
    print_int(a);
    print_string("\n");
}


