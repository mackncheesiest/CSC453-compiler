/*
    output should be equiv to printf("1\n2\n3\n");
*/
extern void print_int(int a);
extern void print_string(char a[]);
void nl(void){print_string("\n");}
void f1(void){print_int(1);}
void f2(void){print_int(2);}
void f3(void){print_int(3);}

void main(void){
    f1();
    nl();
    f2();
    nl();
    f3();
    nl();


}
