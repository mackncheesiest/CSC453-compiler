/*
    Output should be equiv to printf("1\n2\n3\n");
*/

extern void print_int(int a);
extern void print_string(char a[]);

int a;
int b;
int c;
void nl(void){print_string("\n");}
void f1(void){print_int(a);}
void f2(void){print_int(b);}
void f3(void){print_int(c);}

void main(void){
    a = 1;
    b = 2;
    c = 3;
    f1();
    nl();
    f2();
    nl();
    f3();
    nl();


}
