/*      
    Output should be quiv to printf("abcdefg\n1234567\n");
*/
extern void print_int(int a);
extern void print_string(char a[]);

void main(void){
    print_string("abcdefg");
    print_string("\n");
    print_string("1234567");
    print_string("\n");

}
