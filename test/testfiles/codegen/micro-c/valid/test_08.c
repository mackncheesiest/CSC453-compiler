/*
    output should be equiv. to printf("42\n43\n44\n45\n");
*/

extern void print_string(char a[]);
extern void print_int(int a);
void nl(void){print_string("\n");}
void f(void);
void f2(void);
void f3(void);
void f4(void);
int b;

void main(void){
    f();
    print_int(b);
    nl();
}
void f(void){
    f2();
    print_int(b);
    nl();
    b = 45;
}
void f2(void){
    f3();
    print_int(b);
    nl();
    b = 44;
}
void f3(void){
    f4();
    print_int(b);
    nl();
    b = 43;
}
void f4(void){
    b = 42;
    print_int(b);
    nl();
}


