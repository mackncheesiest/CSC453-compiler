extern void print_int(int x);
extern void print_string(char str[]);

/* Arrays aren't supported, but this just ensures that the local variable overrides */
int a[10];

int main(void) {
    int a;
    print_int(5);
    a = 5;
    print_int(a);
    return 0;
}