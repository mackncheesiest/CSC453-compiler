/*
    A normal header comment like you might find at the top of a C file
*/

int myInt;
char myChar;
char myStr[10]; 
extern void myFunc(void);

int main(void) {
    int var1, var2, var3;
    int var4[5];
    var1 = 2;
    var2 = 3;
    if (var1 < var2) {
        myFunc();
    } else {
        var3 = var1 + var2 - var1 * var2 / var1;
        var3 = !var3;
        var3 = -var3;
        if (var1 < var3 || var1 <= var3 && var1 == var3 || var1 >= var3 && var1 > var3) {
            myFunc();
        }
    }
    for (;;) {
        while (var1 == var1) {
            myFunc();
        }
    }
    myChar = 'a';
    char2 = '\n';
    char3 = '\0';
    myStr = "stringcon";
    return var3;
}