/* missing keyword in function formals */
/*
  @ERROR_LINE 15
  @ERROR_LINE 17
  @ERROR_LINE 19
  @ERROR_LINE 21
  @ERROR_LINE 23
  @ERROR_LINE 25
  @ERROR_LINE 27
  @ERROR_LINE 29
  @ERROR_LINE 31
  @ERROR_LINE 33
*/

int f0(int x, y, z) { }

int f1(int x, y, z) { }

int f2(int x, y, z) { }

int f3(int x, y, z) { }

int f4(int x, y, z) { }

int f5(int x, y, z) { }

int f6(int x, y, z) { }

int f7(int x, y, z) { }

int f8(int x, y, z) { }

int f9(int x, y, z) { }
