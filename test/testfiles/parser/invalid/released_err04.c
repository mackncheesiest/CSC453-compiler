/* missing comma in global declaration */
/*
  @ERROR_LINE 10
  @ERROR_LINE 14
  @ERROR_LINE 18
  @ERROR_LINE 22
  @ERROR_LINE 26
*/

int x0 y0, z0;

void f0( void ) { }

int x1 y1, z1;

void f1( void ) { }

int x2 y2, z2;

void f2( void ) { }

int x3 y3, z3;

void f3( void ) { }

int x4 y4, z4;

void f4( void ) { }
