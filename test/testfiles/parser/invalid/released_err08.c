/* missing semicolon after procedure call */
/*
  @ERROR_LINE 12
  @ERROR_LINE 17
  @ERROR_LINE 22
  @ERROR_LINE 27
  @ERROR_LINE 32
*/

int foo0( int x )
{
  foo (x-1)
}

int foo1( int x )
{
  foo (x-1)
}

int foo2( int x )
{
  foo (x-1)
}

int foo3( int x )
{
  foo (x-1)
}

int foo4( int x )
{
  foo (x-1)
}
