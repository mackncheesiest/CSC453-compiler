/* unbalanced braces in function body */
/*
  @ERROR_LINE 23
  @ERROR_LINE 38
  @ERROR_LINE 53
  @ERROR_LINE 68
  @ERROR_LINE 83
*/

int foo0(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }

  }
}

int foo1(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }

  }
}

int foo2(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }

  }
}

int foo3(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }

  }
}

int foo4(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }

  }
}
