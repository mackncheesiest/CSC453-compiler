/* unbalanced braces in function body */
/* 
   @ERROR_LINE 24
   @ERROR_LINE 40
   @ERROR_LINE 56
   @ERROR_LINE 72
   @ERROR_LINE 88
*/

int foo1(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }
    }
  }
}
}

int foo2(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }
    }
  }
}
}

int foo3(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }
    }
  }
}
}

int foo4(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }
    }
  }
}
}

int foo5(int x)
{
  {
    {
      {
	{
	  {
	    x = x+1;
	  }
	}
      }
    }
  }
}
}

