/* unbalanced parentheses in expressions */
/* 
   @ERROR_LINE 19
   @ERROR_LINE 20
   @ERROR_LINE 27
   @ERROR_LINE 28
   @ERROR_LINE 35
   @ERROR_LINE 36
   @ERROR_LINE 43
   @ERROR_LINE 44
   @ERROR_LINE 51
   @ERROR_LINE 52
*/

int foo0(int x)
{
  int a, b, c, d, e, f, g;

  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
}

int foo1(int x)
{
  int a, b, c, d, e, f, g;

  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
}

int foo2(int x)
{
  int a, b, c, d, e, f, g;

  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
}

int foo3(int x)
{
  int a, b, c, d, e, f, g;

  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
}

int foo4(int x)
{
  int a, b, c, d, e, f, g;

  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
  x = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
}
