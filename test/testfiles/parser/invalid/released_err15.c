/* unbalanced brackets in array references */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 18
   @ERROR_LINE 23
   @ERROR_LINE 24
   @ERROR_LINE 29
   @ERROR_LINE 30
   @ERROR_LINE 35
   @ERROR_LINE 36
   @ERROR_LINE 41
   @ERROR_LINE 42
*/

int foo0(int z[])
{
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 1;
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 2;
}

int foo0(int z[])
{
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 1;
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 2;
}

int foo0(int z[])
{
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 1;
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 2;
}

int foo0(int z[])
{
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 1;
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 2;
}

int foo0(int z[])
{
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 1;
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]] = 2;
}
