/* malformed expression */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 22
   @ERROR_LINE 27
   @ERROR_LINE 32
   @ERROR_LINE 37
   @ERROR_LINE 42
   @ERROR_LINE 47
   @ERROR_LINE 52
   @ERROR_LINE 57
   @ERROR_LINE 62
*/

int foo0(int x)
{
  x = y * / z;
}

int foo1(int x)
{
  x = y * / z;
}

int foo2(int x)
{
  x = y * / z;
}

int foo3(int x)
{
  x = y * / z;
}

int foo4(int x)
{
  x = y * / z;
}

int foo5(int x)
{
  x = y * / z;
}

int foo6(int x)
{
  x = y * / z;
}

int foo7(int x)
{
  x = y * / z;
}

int foo8(int x)
{
  x = y * / z;
}

int foo9(int x)
{
  x = y * / z;
}
