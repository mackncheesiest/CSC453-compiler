/* malformed expression */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 22
   @ERROR_LINE 27
   @ERROR_LINE 32
   @ERROR_LINE 37
   @ERROR_LINE 42
   @ERROR_LINE 47
   @ERROR_LINE 52
   @ERROR_LINE 57
   @ERROR_LINE 62
*/

int foo0(int x)
{
  y * / z = x;
}

int foo1(int x)
{
  y * / z = x;
}

int foo2(int x)
{
  y * / z = x;
}

int foo3(int x)
{
  y * / z = x;
}

int foo4(int x)
{
  y * / z = x;
}

int foo5(int x)
{
  y * / z = x;
}

int foo6(int x)
{
  y * / z = x;
}

int foo7(int x)
{
  y * / z = x;
}

int foo8(int x)
{
  y * / z = x;
}

int foo9(int x)
{
  y * / z = x;
}
