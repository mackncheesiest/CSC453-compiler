/* malformed conditional */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 25
   @ERROR_LINE 33
   @ERROR_LINE 41
   @ERROR_LINE 49
*/

int foo0(int x)
{
  if (x > 0
    x = 1;
  else
    x = 2;
}

int foo1(int x)
{
  if (x > 0
    x = 1;
  else
    x = 2;
}

int foo2(int x)
{
  if (x > 0
    x = 1;
  else
    x = 2;
}

int foo3(int x)
{
  if (x > 0
    x = 1;
  else
    x = 2;
}

int foo4(int x)
{
  if (x > 0
    x = 1;
  else
    x = 2;
}
