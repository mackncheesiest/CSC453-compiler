/* malformed conditional */
/* 
   @ERROR_LINE 12
   @ERROR_LINE 20
   @ERROR_LINE 28
   @ERROR_LINE 36
   @ERROR_LINE 44
*/

int foo0(int x)
{
  if x > 0)
    x = 1;
  else
    x = 2;
}

int foo1(int x)
{
  if x > 0)
    x = 1;
  else
    x = 2;
}

int foo2(int x)
{
  if x > 0)
    x = 1;
  else
    x = 2;
}

int foo3(int x)
{
  if x > 0)
    x = 1;
  else
    x = 2;
}

int foo4(int x)
{
  if x > 0)
    x = 1;
  else
    x = 2;
}
