/* malformed conditional */
/* 
   @ERROR_LINE 15
   @ERROR_LINE 23
   @ERROR_LINE 31
   @ERROR_LINE 39
   @ERROR_LINE 47
*/

int foo0(int x)
{
  if (x > 0)
    x = 1;
  else 
    else x = 2;
}

int foo1(int x)
{
  if (x > 0)
    x = 1;
  else 
    else x = 2;
}

int foo2(int x)
{
  if (x > 0)
    x = 1;
  else 
    else x = 2;
}

int foo3(int x)
{
  if (x > 0)
    x = 1;
  else 
    else x = 2;
}

int foo4(int x)
{
  if (x > 0)
    x = 1;
  else 
    else x = 2;
}
