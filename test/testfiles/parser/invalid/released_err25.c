/* malformed conditional */
/* 
   @ERROR_LINE 12
   @ERROR_LINE 18
   @ERROR_LINE 24
   @ERROR_LINE 30
   @ERROR_LINE 36
*/

int foo0(int x)
{
  if x > 0
    x = 1;
}

int foo1(int x)
{
  if x > 0
    x = 1;
}

int foo2(int x)
{
  if x > 0
    x = 1;
}

int foo3(int x)
{
  if x > 0
    x = 1;
}

int foo4(int x)
{
  if x > 0
    x = 1;
}
