/* malformed for loop */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 22
   @ERROR_LINE 27
   @ERROR_LINE 32
   @ERROR_LINE 37
   @ERROR_LINE 42
   @ERROR_LINE 47
   @ERROR_LINE 52
   @ERROR_LINE 57
   @ERROR_LINE 62
*/

int foo0(int x)
{
  for ( ) x = 1;
}

int foo1(int x)
{
  for ( ) x = 1;
}

int foo2(int x)
{
  for ( ) x = 1;
}

int foo3(int x)
{
  for ( ) x = 1;
}

int foo4(int x)
{
  for ( ) x = 1;
}

int foo5(int x)
{
  for ( ) x = 1;
}

int foo6(int x)
{
  for ( ) x = 1;
}

int foo7(int x)
{
  for ( ) x = 1;
}

int foo8(int x)
{
  for ( ) x = 1;
}

int foo9(int x)
{
  for ( ) x = 1;
}
