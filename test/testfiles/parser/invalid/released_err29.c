/* malformed for loop */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 23
   @ERROR_LINE 29
   @ERROR_LINE 35
   @ERROR_LINE 41
   @ERROR_LINE 47
   @ERROR_LINE 53
   @ERROR_LINE 59
   @ERROR_LINE 65
   @ERROR_LINE 71
*/

int foo0(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo1(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo2(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo3(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo4(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo5(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo6(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo7(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo8(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}

int foo9(int x)
{
  for (x=0; x < 10; x += 1) 
    ;
}
