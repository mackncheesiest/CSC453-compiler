/* malformed while loop */
/* 
   @ERROR_LINE 17
   @ERROR_LINE 23
   @ERROR_LINE 29
   @ERROR_LINE 35
   @ERROR_LINE 41
   @ERROR_LINE 47
   @ERROR_LINE 53
   @ERROR_LINE 59
   @ERROR_LINE 65
   @ERROR_LINE 71
*/

int foo0(int x)
{
  while (x>)
    ;
}

int foo1(int x)
{
  while (x>)
    ;
}

int foo2(int x)
{
  while (x>)
    ;
}

int foo3(int x)
{
  while (x>)
    ;
}

int foo4(int x)
{
  while (x>)
    ;
}

int foo5(int x)
{
  while (x>)
    ;
}

int foo6(int x)
{
  while (x>)
    ;
}

int foo7(int x)
{
  while (x>)
    ;
}

int foo8(int x)
{
  while (x>)
    ;
}

int foo9(int x)
{
  while (x>)
    ;
}
