/* malformed while loop */
/* 
   @ERROR_LINE 19
   @ERROR_LINE 25
   @ERROR_LINE 31
   @ERROR_LINE 37
   @ERROR_LINE 43
   @ERROR_LINE 49
   @ERROR_LINE 55
   @ERROR_LINE 61
   @ERROR_LINE 67
   @ERROR_LINE 73
*/

int foo0(int x)
{
  while (x > 0) {
    ;
}

int foo1(int x)
{
  while (x > 0) {
    ;
}

int foo2(int x)
{
  while (x > 0) {
    ;
}

int foo3(int x)
{
  while (x > 0) {
    ;
}

int foo4(int x)
{
  while (x > 0) {
    ;
}

int foo5(int x)
{
  while (x > 0) {
    ;
}

int foo6(int x)
{
  while (x > 0) {
    ;
}

int foo7(int x)
{
  while (x > 0) {
    ;
}

int foo8(int x)
{
  while (x > 0) {
    ;
}

int foo9(int x)
{
  while (x > 0) {
    ;
}
