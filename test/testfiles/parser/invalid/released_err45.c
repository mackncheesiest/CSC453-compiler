/* malformed while loop */
/* 
   @ERROR_LINE 18
   @ERROR_LINE 24
   @ERROR_LINE 30
   @ERROR_LINE 36
   @ERROR_LINE 42
   @ERROR_LINE 48
   @ERROR_LINE 54
   @ERROR_LINE 60
   @ERROR_LINE 66
   @ERROR_LINE 72
*/

int foo0(int x)
{
  while (x > 0)
  }
}

int foo1(int x)
{
  while (x > 0)
  }
}

int foo2(int x)
{
  while (x > 0)
  }
}

int foo3(int x)
{
  while (x > 0)
  }
}

int foo4(int x)
{
  while (x > 0)
  }
}

int foo5(int x)
{
  while (x > 0)
  }
}

int foo6(int x)
{
  while (x > 0)
  }
}

int foo7(int x)
{
  while (x > 0)
  }
}

int foo8(int x)
{
  while (x > 0)
  }
}

int foo9(int x)
{
  while (x > 0)
  }
}
