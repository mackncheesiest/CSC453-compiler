/* malformed for+while loop */
/* 
   @ERROR_LINE 18
   @ERROR_LINE 25
   @ERROR_LINE 32
   @ERROR_LINE 39
   @ERROR_LINE 46
   @ERROR_LINE 53
   @ERROR_LINE 60
   @ERROR_LINE 67
   @ERROR_LINE 74
   @ERROR_LINE 81
*/

int foo0(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo1(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo2(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo3(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo4(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo5(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo6(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo7(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo8(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}

int foo9(void)
{
  for (x = 0; x < 10; x = x+1 {
      while x > 0)
    }
}
