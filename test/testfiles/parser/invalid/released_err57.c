/* unbalanced brackets in function prototype */
/* 
   @ERROR_LINE 15
   @ERROR_LINE 18
   @ERROR_LINE 21
   @ERROR_LINE 24
   @ERROR_LINE 27
   @ERROR_LINE 30
   @ERROR_LINE 33
   @ERROR_LINE 36
   @ERROR_LINE 39
   @ERROR_LINE 42
*/

int foo0(int x[, int y, char z[);


int foo1(int x[, int y, char z[);


int foo2(int x[, int y, char z[);


int foo3(int x[, int y, char z[);


int foo4(int x[, int y, char z[);


int foo5(int x[, int y, char z[);


int foo6(int x[, int y, char z[);


int foo7(int x[, int y, char z[);


int foo8(int x[, int y, char z[);


int foo9(int x[, int y, char z[);

