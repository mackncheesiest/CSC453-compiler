/* weirdnesses that are OK for syntax analysis but won't be once we
   have type information. */

/* Modifications by Josh: changed to an error testcase now that type information is involved */

int foo( void )
{
  a["foo"] = "bar";
  foo['\n'] = bar[foo["foo"]];
  foo = "bar" + 1;
}
