/* multiple function definitions, none of which do much */

void a0( void ) {}

int a1( int x ) { return 0; }

void a2( int x, int y, int z ) {}

int a3( int x[], char y, int z[], char w[] ) { return 1; }

char a4( int x[], char y, int z[], char w[] ) { return 'a'; }

