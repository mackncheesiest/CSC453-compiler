/* function definitions involving local scalar variables */

void foo0( void )
{
  int x;
  int y;
  int z;
  char a;
  char b;
}

void foo1( void )
{
  int x, y, z;
  char a, b;
}

int foo2( void )
{
  int x, y, z;
  char a, b;
  return -((((((((x)+y)*z)/a)-b)*x)-b));
}

int foo3( int xx, int yy, char zz )
{
  int x, y, z;
  char a, b;
  return 0;
}

char foo4( int xx, int yy )
{
  int x, y, z;
  char a, b;
  return a+b;
}

