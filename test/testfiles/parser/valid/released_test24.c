/* function definitions with array formals */

void foo0( int u[] ) { }

void foo1( char v[] ) {}

int foo2( int w[] ) { return w[0]; }

int foo3( int xx[], int yy[], char zz[] ) { return yy[0] * zz[1]; }

char foo4( int xx[], int yy[] ) { return 'o';}

