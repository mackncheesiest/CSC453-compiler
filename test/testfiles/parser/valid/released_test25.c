/* function definitions with mixed scalar and array formals */

void foo0( int u[], int v ) { }

void foo1( char v[], int w, int x ) {}

int foo2( int x, int w[], char v, int w1[], char a ) { return x; }

int foo3( int xx[], int yy, char zz[] ) { return zz[xx[yy]] + xx[zz[yy]];}

char foo4( int xx, int yy[] ) { return yy[xx]/(xx+yy[xx]); }

