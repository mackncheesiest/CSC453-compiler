/* function definitions with mixed scalar and array formals,
   and local scalar and array declarations */

void foo0( int u[], int v )
{ 
  int x, y, z;
  int u1[10], v1, w[10];
}

void foo1( char v[], int w, int x ) 
{
  char a[10], b, c, d, e[10], f[10], g[20], h;
  int x1, y, xx, yy, xxx[10], yyy[20], xxxx[30];
  int yyyy, zzz;
}

int foo2( int x, int w[], char v, int w1[], char a ) 
{
  char a1[10], b, c, d, e[10], f[10], g[20], h;
  int x1, y, xx, yy, xxx[10], yyy[20], xxxx[30];
  int yyyy, zzz;
  return yyyy;
}

int foo3( int xx[], int yy, char zz[] ) 
{
  char a[10], b, c, d, e[10], f[10], g[20], h;
  int x, y, xx1, yy1, xxx[10], yyy[20], xxxx[30];
  int yyyy, zzz;
  return zzz;
}

char foo4( int xx, int yy[] ) 
{
  char a[10], b, c, d, e[10], f[10], g[20], h;
  int x, y, xx1, yy1, xxx[10], yyy[20], xxxx[30];
  int yyyy, zzz;
  return yyyy+zzz;
}

