/* simple statements involving array references */

extern int printThing(char str[]);

int x, y[10];

int foo(int u, int v[], char w)
{
  int l0, l1[10], l2;
  char myStr[30];

  y[5] = 0;
  l0 = l1[1];
  x = l0;
  x = 1;
  l1[1] = 2;
  y[x] = x;
  l1[x] = y[l0];
  l1[u] = '\0'/printThing("this is a string constant");
  y[l2] = 't'*printThing("this also\n is a %string constant\n");
  v[x] = '\n';
  return y[l1[myStr[w]]];
}
