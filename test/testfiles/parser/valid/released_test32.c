/* assignments involving more complex expressions */

int x, y, z[10];

int foovoid(void);
int foo1(int oneNum);
int foo2(int one, int two);
int foo3(int one, int two, int three);
int foo4(int one, int two, int three, int four);

void foo( int a, int b, char c, int d[], char e[], char f, char g0[])
{
  int l0, l1[10], l2, l3;
  int z0; char g;
  x = -1;
  x = 1+1;
  x = 1+1+1;
  x = 1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+-1+-2+-3+-4;
  x = y;
  x = y+z0*y+z0*y+z0*y+z0*y+z0*y+z0*y+z0*y+z0*y+z0*y+z0*y;
  z[0] = 0;
  z[1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1] = 0;
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] = 1;
  a = z[0];
  a = z[1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1];
  a = z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]];
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] = 1;
  a = z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] + z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]];
  a = z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] + z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] * z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] / z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]];
  a = (z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] + z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]]) * z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] / z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]];
  z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[x]]]]]]]]]]]]]]]]] = (z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] + z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]]) * z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]] / z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]]]]]]]];
  l0 = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))));
  l2 = a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g))))))))/(a+2*(a-3*(b+4*(b-5*(c+6*(c-7*(x+8*(x-f+9*(f-g)))))))));
  l2 = foovoid();
  x = foo1(1);
  y = foo2(1,1);
  y = foo3(1,-1,1);
  y = foo1(-x);
  x = foo1(1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1);
  y = foo1(x);
  y = foo3(x, y, z[x]);
  l0 = foo1(y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+-z[y+x]*-y);
  l0 = foo2(y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y+z[y+x]*y, 1+2+3+4+5+6+7+8+9+-1*-2/-3);
  l2 = foo1(z[0]);
  l2 = foo3(z[0], z[1], z[2]);
  l2 = foo1(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  l2 = foo3(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  l2 = foo1(foo1(foo1(foo1(foo1(foo1(0))))));
  l2 = foo1(foo1(foo1(foo1(foo1(foo1(0+1+2+3+4+5+6+7+8+9+x))))));
  l2 = foo1(foo1(foo1(foo1(foo1(foo1(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]))))));
  l2 = foo1(foo1(foo1(foo1(foo1(foo1(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]+1-'\n'))))));
  l2 = z[z[z[z[z[z[foo1(foo1(foo1(foo1(foo1(foo1(foo1(foo1(foo1(foo1(0))))))))))+1-'\n']]]]]];
}
