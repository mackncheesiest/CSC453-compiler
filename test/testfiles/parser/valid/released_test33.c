/* procedure calls */

int x, y, z[10];

extern void bar(int val), foo(int val2, int somethingElse);
extern int barint(int val);

void foo0( void )
{
  bar(x);
  foo(x, y);
  bar(z[0]);
}

void foo1( int x, int y )
{
  bar(x);
  foo(x, y);
  bar(z[0]);
}

int foo2( int x, int y )
{
  int u, v, w;

  bar(w);
  foo(u, y);
  return barint(z[0]);
}

