/* procedure calls involving complicated actuals */

/* functions with assignments involving more complex expressions */

int x, y, z[10];

extern int bar(int x);
extern int bar2(int x, int y);
extern int bar4(int x, int y, int z, int w);

int foovoid(void);
void foo1(int oneNum);
int foo2(int one, int two);
int foo3(int one, int two, int three);

void foo( int a, int b, char c, int d[], char e[], char f, char g[])
{
  int l0, l1[10], l2, l3;
  
  foo1(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  foo1(bar4(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], 1));
  foo1(bar(bar(bar(bar(bar(bar(0)))))));
  foo1(bar(bar(bar(bar(bar(bar(0+1*2+3/4+5-6*7/8+9*x)))))));
  foo1(bar(bar(bar(bar(bar(bar(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]])))))));
  foo1(bar(bar(bar(bar(bar(bar(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]+1-'\n')))))));
}
