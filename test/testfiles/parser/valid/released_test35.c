/* return statements */

int x, y, z[10];

extern int bar1(int I);
extern int bar(int x, int y);
extern int bar4(int i, int ii, int iii, int iiii);
int foo4(int one, int wo, int tr, int fr);

int foo( int x, int y, char z)
{
  return x;
}

int foo0(int x)
{
  return x;
}

int foo1(int x, int y)
{
  return x+y;
}

int foo2(int x, char y, int z)
{
  return x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*x+x*2;
}

char foo3( int a, int b, char c, int d[], int e[], char f, char g[])
{
  int l0, l1[10], l2, l3;
  
  return foo(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  return foo(bar4(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], 1), z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  return foo(bar1(bar1(bar1(bar1(bar1(bar1(0)))))), z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  return foo(bar1(bar1(bar1(bar1(bar1(bar1(0+1*2+3/4+5-6*7/8+9*x)))))), z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  return foo(bar1(bar1(bar1(bar1(bar1(bar1(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]])))))), z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
  return foo(bar1(bar1(bar1(bar1(bar1(bar1(z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]+1-'\n')))))), z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]], z[z[z[z[z[z[z[z[z[z[0]]]]]]]]]]);
}

int foo4(int one, int tw, int trr, int forr) {
  return one + tw + trr / forr;
}