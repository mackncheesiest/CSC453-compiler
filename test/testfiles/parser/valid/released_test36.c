/* simple conditionals */

int x, y, z[10];

int foo0( void )
{
  if (1 > 0) 
    x = 1;

    return x;
}

int foo1( void )
{
  if (1 >= 0) 
    x = 1;
  else 
    x = 2;

    return x + 1;
}

int foo2( int u )
{
  if (u < 0) 
    x = 1;
  else 
    x = 2;
    return u - x;
}

int foo3( int u )
{
  if (u <= x) 
    x = 1;
  else 
    x = 2;
    return u/x/u/x/u/x*u;
}


int foo5( int u )
{
  if (u != x) 
    x = 1;
  else 
    x = 2;
    return x*x*x*x/x/x/x/x/x/x/x/x/x/x/x/x/x*u/x+u+x+u+x+2*u;
}

int foo6( int u )
{
  if (!(u != x))
    x = 1;
  else 
    x = 2;
    return foo5(u);
}
