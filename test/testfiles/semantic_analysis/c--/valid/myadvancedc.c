int x;
int y, z[10];

void myFunction(void);
int func2(int x, int y, int z, int w), func3(int a, int b, char c);

void myFunction(void) {
    int a;
    a = 5;
    x = 6;
    y = 7;
    z[0] = y + x + a;
}

int func2(int x2, int y, int z, int w2) {
    char thing[5];
    thing[0] = 2;
    x2 = y + z + w2;
    return x2;
}

/*
Symbol Table:

1.
head -> x[type: int]


2.
head -> x[type: int] -> y[type: int]


3.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10]


4.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void]
  ^
  |
  v
myFunction[type: func, argTypes: void, retType: void]


5.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void]


6.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int]


7.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int]


8.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int]


9.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int] -> z[type: int]


10.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int] -> z[type: int] -> w[type: int]


11. 
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]


12.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  V
myFunction[type: func, argTypes: void, retType: void]


13.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  V
myFunction[type: func, argTypes: void, retType: void] -> a[type: int]


14.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int] 


15.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int]


16.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int]


17.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int]


18.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int] -> z[type: int]


19.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int] -> z[type: int] -> w[type: int]


20.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]
  ^
  |
  v
func2[type: func, argTypes: [int, int, int, int], retType: int] -> x[type: int] -> y[type: int] -> z[type: int] -> w[type: int] -> thing[type: chararr, numArrElem: 5]


21.
head -> x[type: int] -> y[type: int] -> z[type: int, numArrElem: 10] -> myFunction[type: func, argTypes: void, retType: void] -> func2[type: func, argTypes: [int, int, int, int], retType: int]

Takeaways:
For variables, do like we currently do (allocate on whatever is the current "st" head table)
For functions
  (i) Check that we're at the global symbol table (i.e. prevTable == NULL). Function declarations can only occur there
  (ii) Construct one symbol to add to the global symbol table (but don't actually add it yet! Might need to store in a temporary global ptr)
  (iii) Construct another identical symbol (but a different malloc/ptr!) and push it onto the symbol table stack, making it the current st head
  (iv) As you parse the args:
    (a) Construct their symbols and add them to the current local symbol table (i.e. the one referred to by ptr-(iii)). Throw an error if any redeclarations are made in the scope of ptr-(iii). 
    (b) Add the types of their symbols to the placeholder global symbol table's symbol (i.e. the one referred to by ptr-(ii))
  (v) When you reach the closing right paren:
    (a) If this was a declaration (i.e. a semicolon follows):
      (*) If a symbol with this identifier already exists in the global symbol table, you aren't allowed to redeclare/reprototype functions. Pop the table referred to by ptr-(iii). Throw an error.
      (**) If a symbol with this identifier doesn't exist in the global symbol table, append ptr-(ii) with type FUNCTION_NODEF_TYPE. Pop the table referred to by ptr-(iii)
    (b) If this was a definition (i.e. a curly brace follows):
      (*) If a symbol with this identifier exists in the global symbol table and it's of type FUNCTION_HASDEF_TYPE, then throw an error. Free ptr-(ii). Don't pop the ptr-(iii) table
      (**) If a symbol with this identifier exists in the global symbol table and it's of type FUNCTION_NODEF_TYPE and the arg types don't match, then throw an error. Free ptr-(ii). Don't pop the ptr-(iii) table
      (***) If a symbol with this identifier exists in the global symbol table and it's of type FUNCTION_NODEF_TYPE and the arg types match, then continue. Free ptr-(ii). Don't pop the ptr-(iii) table
      (****) If no symbol with this identifier exists in the global symbol table, append ptr-(ii) to the global symbol table with type FUNCTION_HASDEF_TYPE and continue. Don't pop the ptr-(iii) table
  (vi) For function prototypes, exit
  (vii) For function definitions, handle the local scope from the context of the current local symbol table (what should be the st head)
  (viii) When you reach the closing curly brace, pop the current st head to return to the context of the global symbol table
*/