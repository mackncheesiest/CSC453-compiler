/* multiple declarations of a global variable */

int x;

void foo(int y)
{
}

char x[10];  /* x already declared as global */

void bar(int y)
{
}
