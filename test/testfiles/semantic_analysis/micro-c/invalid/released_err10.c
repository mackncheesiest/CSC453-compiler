/* actual parameter in function call not compatible with formal */

int x[10], a;

void foo(int u)
{
  foo(x);
}
