/* RHS-type of assignment not compatible with LHS-type */

int x[10], y, z;

void foo(void)
{
  y = x;
}
