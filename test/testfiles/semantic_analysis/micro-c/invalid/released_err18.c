/* function prototype doesn't match definition: types of args */

void f(int x);

void f(char x)
{
}

void bar(int x[]);

void bar(int x)
{
}
