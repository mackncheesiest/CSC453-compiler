/* variables: declaration before use */

int glob1, glob2;

void foo(int loc1)
{
  loc1 = glob1;
  glob2 = loc1;
}

