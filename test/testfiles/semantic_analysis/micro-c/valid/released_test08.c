/* variables: scope distinctions for the same name */

int x, y, z[10];

void f(int z)
{
  z = 10;
}

void g(char g)
{
  g = 20;
}

