/* string constants and array parameters */

int a[10];

void bar(char y[]);

void foo(char x[])
{
  foo("foo");
  bar("bar");
}
