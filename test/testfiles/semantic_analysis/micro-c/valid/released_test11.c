/* simple expressions */

int x[10], y;

void bar(int x)
{
  x = 10;
  y = x;
  x = y;
}
